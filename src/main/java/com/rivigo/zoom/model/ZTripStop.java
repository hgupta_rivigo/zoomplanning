package com.rivigo.zoom.model;

import com.rivigo.zoom.model.common.trip.ZTrip;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by hgupta on 11/8/16.
 */
@Getter
@Setter
public class ZTripStop implements Comparable<ZTripStop>{
    private ZTrip zTrip;
    private ZLoc zLoc;
    private int seq;
    private long arrivalTime = -1l;
    private long departureTime = -1l;

    public String getLocCode() {
        return zLoc.getLoc();
    }

    public ZTripStop(int seq, String loc, ZTrip trip) {
        this.zTrip = trip;
        this.seq = seq;
        this.zLoc = new ZLoc(loc);
    }

    public int compareTo(ZTripStop tripStop) {
        if(this.zTrip.equals(tripStop.getZTrip())) {
            return this.seq - tripStop.seq;
        }

        return this.zTrip.getTripCode().compareTo(tripStop.getZTrip().getTripCode());
    }
}
