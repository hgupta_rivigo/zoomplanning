package com.rivigo.zoom.model.output;

import com.rivigo.zoom.ZParams;
import com.rivigo.zoom.model.TripLeg;
import com.rivigo.zoom.input.dto.ClientPromisedTat;
import com.rivigo.zoom.utils.TimeUtils;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang.StringUtils;
import org.joda.time.LocalTime;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by hgupta on 29/7/16.
 */

@Getter
@Setter
public class PickupDeliveryAnalyserFormat1 implements Comparable<PickupDeliveryAnalyserFormat1> {
    public String client;
    public String cnoteSrc;
    public String cnoteDest;
    public LocalTime pickupCutoff;
    public int tatDays;
    public LocalTime deliverCutoff;

    public String option = "NO OPTIONS";

    public String plan = "-";
    public int transShipments = 0;
    public String planStartTime = "-";
    public int tatHours;
    public String planEndTIme = "-";

    public float pickupBuffer;
    public float deliveryBuffer;
    public String transShipmentBuffer;

    public String isPickupOk = "NO";
    public String isDeliveryOk = "NO";
    public String isTatOk = "NO";
    public String isTranshipmentOk = "-";

    public PickupDeliveryAnalyserFormat1(ClientPromisedTat cpt) {
        this.client = cpt.getClient();
        this.cnoteSrc = cpt.getSrcPC();
        this.cnoteDest = cpt.getDestPC();
        this.pickupCutoff = cpt.getPickupTime();
        this.tatDays = cpt.getTatDays();
        this.deliverCutoff =  cpt.getDeliveryTime();
    }

    public PickupDeliveryAnalyserFormat1(ClientPromisedTat cpt, List<TripLeg> solution, String option) {
        this(cpt);
        this.option = option;

        if (solution != null && !solution.isEmpty()) {
            this.plan = createPlanString(solution);
            this.transShipments = solution.size() - 1;
            this.planStartTime = solution.get(0).getStartTime();
            this.planEndTIme = solution.get(solution.size()-1).getEndTime();

            long routeTatMillis = calculateTat(solution);
            long routeTatWithoutTranshipmentMillis = calculateTatWithoutTranshipment(solution);
            long pickupBufferMillis = calculatePickupBuffer(cpt, solution);
            long deliveryBufferMillis = calculateDeliverBuffer(cpt, solution);

            this.tatHours = (int) (routeTatMillis/ TimeUtils.MILLIS_IN_HOUR);
            pickupBuffer = (int) (pickupBufferMillis/ TimeUtils.MILLIS_IN_HOUR);
            deliveryBuffer = (int) (deliveryBufferMillis/ TimeUtils.MILLIS_IN_HOUR);

            List<String> tsStrings = new ArrayList<String>();
            if(this.transShipments > 0) {
                for(int i=0; i<solution.size()-1; i++) {
                    TripLeg prev = solution.get(i);
                    TripLeg next = solution.get(i + 1);
                    int f = (int) ((next.getStartTimeMillis() - (prev.getStartTimeMillis() + prev.getTravelTimeMillis())) / TimeUtils.MILLIS_IN_HOUR);
                    tsStrings.add(prev.getDestLoc() + ":" + f);
                    if(next.getStartTimeMillis() - (prev.getStartTimeMillis() + prev.getTravelTimeMillis() ) >= 2 * 3600 * 1000l
                            && !this.isTranshipmentOk.equals("NO")) {
                        this.isTranshipmentOk = "YES";
                    } else {
                        this.isTranshipmentOk = "NO";
                    }
                }
            }
            transShipmentBuffer = StringUtils.join(tsStrings, ",");

            this.isTatOk = "YES";
            if(cpt.getTotalTransitTimeInMillis() - ZParams.MIN_TAT_BUFFER_FOR_VALID_CPT_MINS * TimeUtils.MILLIS_IN_MINUTE < routeTatWithoutTranshipmentMillis) {
                this.isTatOk = "NO";
                this.isPickupOk = "-";
                this.isDeliveryOk = "-";
            } else  if(pickupBufferMillis >= ZParams.MIN_PICKUP_BUFFER_MINS * TimeUtils.MILLIS_IN_MINUTE
                    && deliveryBufferMillis >= ZParams.MIN_DELIVERY_BUFFER_MINS * TimeUtils.MILLIS_IN_MINUTE) {
                this.isPickupOk = "YES";
                this.isDeliveryOk = "YES";
            } else if(pickupBufferMillis >= ZParams.MIN_PICKUP_BUFFER_MINS * TimeUtils.MILLIS_IN_MINUTE
                    && deliveryBufferMillis >= 0) {
                this.isPickupOk = "YES";
                this.isDeliveryOk = "NO";
            } else if(pickupBufferMillis > 0
                    && deliveryBufferMillis >= ZParams.MIN_DELIVERY_BUFFER_MINS * TimeUtils.MILLIS_IN_MINUTE) {
                this.isPickupOk = "NO";
                this.isDeliveryOk = "YES";
            }else if(pickupBufferMillis >= 0
                    && deliveryBufferMillis >= 0) {
                this.isPickupOk = "NO";
                this.isDeliveryOk = "NO";
            } else if(pickupBufferMillis > 12 * TimeUtils.MILLIS_IN_HOUR) {
                this.isPickupOk = "NO"; //linehaul is leaving way too late. Buffer is getting consumed at pickup.
                this.isDeliveryOk = "YES";
            } else {
                this.isPickupOk = "YES";
                this.isDeliveryOk = "NO";
            }
        }
    }

    private long calculateDeliverBuffer(ClientPromisedTat cpt, List<TripLeg> solution) {
        long deliveryTime = cpt.getTotalTransitTimeInMillis() + cpt.pickupTime.getMillisOfDay();
        TripLeg endLeg = solution.get(solution.size() - 1);
        long tripEndTime = endLeg.getStartTimeMillis() + endLeg.travelTimeMillis;
        long buffer = deliveryTime - tripEndTime;
        return buffer;
    }

    private long calculatePickupBuffer(ClientPromisedTat cpt, List<TripLeg> solution) {
        TripLeg startLeg = solution.get(0);
        long startLegStartTime = startLeg.getStartTimeMillis();
        long pickupTimeMillis = cpt.getPickupTime().getMillisOfDay();
        long buffer = startLegStartTime - pickupTimeMillis;
        return buffer;
    }

    private long calculateTat(List<TripLeg> solution) {
        TripLeg startLeg = solution.get(0);
        long tripStartTime = startLeg.getStartTimeMillis();
        TripLeg endLeg = solution.get(solution.size() - 1);
        long tripEndTime = endLeg.getStartTimeMillis() + endLeg.travelTimeMillis;

        long tat = tripEndTime - tripStartTime;
        return tat;
    }

    private String createPlanString(List<TripLeg> solution) {
        List<String> str = new ArrayList();
        for(TripLeg rl : solution) {
            str.add(rl.getTripId());
        }
        return StringUtils.join(str, ", ");
    }

    public int compareTo(PickupDeliveryAnalyserFormat1 pickupDeliveryAnalyserFormat1) {
        int ret = this.client.compareTo(pickupDeliveryAnalyserFormat1.client);
        if(ret != 0)
            return ret;

        ret = this.cnoteSrc.compareTo(pickupDeliveryAnalyserFormat1.cnoteSrc);
        if(ret != 0)
            return ret;

        ret = this.cnoteDest.compareTo(pickupDeliveryAnalyserFormat1.cnoteDest);
        if(ret != 0)
            return ret;

        return this.option.compareTo(pickupDeliveryAnalyserFormat1.option);
    }
    private long calculateTatWithoutTranshipment(List<TripLeg> solution) {
        long retVal = 0l;
        for(TripLeg rl : solution) {
            retVal += rl.getTravelTimeMillis();
        }
        return retVal;
    }
}