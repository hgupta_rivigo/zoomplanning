package com.rivigo.zoom.model.output;

import com.rivigo.zoom.model.common.route.ZRoute;
import lombok.Getter;
import lombok.Setter;
import org.joda.time.LocalTime;

/**
 * Created by hgupta on 8/8/16.
 */

@Getter
@Setter
public class RouteSelectionAlgoOutput {
    private int seq;
    private String routeSelected;
    private LocalTime startTime;
    private int newCptCount;
    private int selectedCptCount;
    private int totalCptCount;

    public RouteSelectionAlgoOutput(int seq, ZRoute route,long startTime, int newCptCount, int selectedCptCount, int totalCptCount) {
        this.seq = seq;
        this.routeSelected = route.getRouteCode();
        this.startTime = new LocalTime(startTime);
        this.newCptCount = newCptCount;
        this.selectedCptCount = selectedCptCount;
        this.totalCptCount = totalCptCount;
    }
}
