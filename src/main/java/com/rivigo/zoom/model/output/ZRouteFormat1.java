package com.rivigo.zoom.model.output;

import com.rivigo.zoom.model.common.route.ZRoute;
import com.rivigo.zoom.utils.TimeUtils;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by hgupta on 8/8/16.
 */
@Getter
@Setter
public class ZRouteFormat1 {
    public String routeCode;
    public int tat;

    public ZRouteFormat1(ZRoute r) {
        routeCode = r.getRouteCode();
        tat = (int) (r.getTotalTat() / TimeUtils.MILLIS_IN_HOUR);
    }
}
