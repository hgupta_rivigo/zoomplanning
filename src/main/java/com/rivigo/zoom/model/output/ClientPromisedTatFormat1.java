package com.rivigo.zoom.model.output;

import com.rivigo.zoom.input.dto.ClientPromisedTat;
import com.rivigo.zoom.utils.TimeUtils;
import lombok.Getter;
import lombok.Setter;
import org.joda.time.LocalTime;

/**
 * Created by hgupta on 8/8/16.
 */
@Getter
@Setter
public class ClientPromisedTatFormat1 {
    public String client;
    public String srcPC;
    public String destPC;
    public LocalTime pickupTime;
    public int tatDays;
    public LocalTime deliveryTime;
    public double loadKg;
    public int priority;
    public int tatHours;
    public double minTatHours;
    public boolean validBuffer = true;
    public int noOfRoutes = 0;
    
    public ClientPromisedTatFormat1(ClientPromisedTat cpt, double minTatHours, boolean validBuffer) {
        this.client = cpt.client;
        this.srcPC = cpt.srcPC;
        this.destPC = cpt.destPC;
        this.pickupTime = cpt.pickupTime;
        this.tatDays = cpt.tatDays;
        this.deliveryTime = cpt.deliveryTime;
        this.loadKg = cpt.loadKg;
        this.priority = cpt.priority;
        this.tatHours = (int) (cpt.getTotalTransitTimeInMillis()/ TimeUtils.MILLIS_IN_HOUR);
        this.minTatHours = minTatHours;
        this.validBuffer = validBuffer;
    }
    public ClientPromisedTatFormat1(ClientPromisedTat cpt, int noOfRoutes) {
        this.client = cpt.client;
        this.srcPC = cpt.srcPC;
        this.destPC = cpt.destPC;
        this.pickupTime = cpt.pickupTime;
        this.tatDays = cpt.tatDays;
        this.deliveryTime = cpt.deliveryTime;
        this.loadKg = cpt.loadKg;
        this.priority = cpt.priority;
        this.tatHours = (int) (cpt.getTotalTransitTimeInMillis()/ TimeUtils.MILLIS_IN_HOUR);
        this.noOfRoutes = noOfRoutes;
    }
}
