package com.rivigo.zoom.model.output;

import com.rivigo.zoom.model.TripLeg;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by hgupta on 29/7/16.
 */
@Getter
@Setter
public class RouteLegFormat implements Comparable<RouteLegFormat> {
    public int startDayOfTrip;
    public String routeCode;

    public String srcLoc;
    public String destLoc;
    public int legNumber;


    public String startTime;
    public String tat;
    public String endTime;

    public RouteLegFormat(TripLeg rl) {
        this.startDayOfTrip = rl.startDayOfTrip;
        this.routeCode = rl.routeCode;
        this.srcLoc = rl.srcLoc;
        this.destLoc = rl.destLoc;
        this.startTime = rl.getStartTime();
        this.tat = rl.getTat();
        this.endTime = rl.getEndTime();
    }

    public int compareTo(RouteLegFormat routeLeg) {
        if(this.routeCode.equals(routeLeg.routeCode)) {
            return this.startDayOfTrip - routeLeg.startDayOfTrip;
        }

        return this.routeCode.compareTo(routeLeg.routeCode);
    }

}
