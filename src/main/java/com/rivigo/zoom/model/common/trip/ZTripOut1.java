package com.rivigo.zoom.model.common.trip;

import com.rivigo.zoom.utils.TimeUtils;
import lombok.Getter;
import lombok.Setter;
import org.joda.time.LocalTime;

/**
 * Created by hgupta on 11/8/16.
 */
@Getter
@Setter
public class ZTripOut1 {
    private String route;
    private LocalTime dispatchTime;

    public ZTripOut1(ZTrip trip) {
        dispatchTime = TimeUtils.getLocalTime(trip.getDispatchTime());
        route = trip.getRoute().getRouteCode();
    }
}
