package com.rivigo.zoom.model.common.trip;

import com.rivigo.common.report.impl.GenericReportGeneratorImpl;
import com.rivigo.zoom.model.ZTripStop;
import com.rivigo.zoom.utils.TimeUtils;
import lombok.Getter;
import lombok.Setter;
import org.apache.poi.ss.usermodel.Sheet;
import org.joda.time.LocalTime;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hgupta on 11/8/16.
 */
@Getter
@Setter
public class ZTripStopOut1 {
    private String tripCode;
    private String loc;
    private int seq = -1;
    private LocalTime arrivalTime;
    private LocalTime departureTime;

    public ZTripStopOut1(ZTripStop tripStop) {
        this.tripCode = tripStop.getZTrip().getTripCode();
        this.loc = tripStop.getLocCode();
        this.seq = tripStop.getSeq();
        if(tripStop.getArrivalTime() >= 0)
            this.arrivalTime = TimeUtils.getLocalTime(tripStop.getArrivalTime());

        if(tripStop.getDepartureTime() >= 0)
            this.departureTime = TimeUtils.getLocalTime(tripStop.getDepartureTime());
    }

    public static void publishTrips(List<ZTrip> trips, Sheet sh) {
        List<ZTripStopOut1> out = new ArrayList<ZTripStopOut1>();

        int i = 0;
        for(ZTrip trip : trips) {
            List<ZTripStop> stops = trip.getTripStops();
            for(ZTripStop stop : stops) {
                out.add(new ZTripStopOut1(stop));
            }
        }

        GenericReportGeneratorImpl.write(sh, ZTripStopOut1.class, out);

    }
}
