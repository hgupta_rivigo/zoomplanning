package com.rivigo.zoom.model.common.trip;

import com.rivigo.zoom.ZCache;
import com.rivigo.zoom.model.ZLoc;
import com.rivigo.zoom.model.ZRouteStop;
import com.rivigo.zoom.model.ZTripStop;
import com.rivigo.zoom.model.common.route.ZRoute;
import lombok.Getter;

/**
 * Created by hgupta on 11/8/16.
 */
@Getter
public class ZTripLeg {
    ZTrip trip;
    public ZLoc startLoc;
    public ZLoc endLoc;
    public long startLocDispatchTime;
    public long endLocArrivalTime;

    public ZTripLeg(ZTrip trip, ZRouteStop startLoc, ZRouteStop endLoc,
                    long startLocDispatchTime, long endLocArrivalTime) {
        this.trip = trip;
        this.startLoc = ZCache.getZLoc(startLoc.getLocCode());
        this.endLoc = ZCache.getZLoc(endLoc.getLocCode());
        this.startLocDispatchTime = startLocDispatchTime;
        this.endLocArrivalTime = endLocArrivalTime;
    }
}
