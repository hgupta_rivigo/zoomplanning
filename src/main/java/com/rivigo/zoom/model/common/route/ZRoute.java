package com.rivigo.zoom.model.common.route;

import com.rivigo.zoom.ZParams;
import com.rivigo.zoom.lhscheduleoptimiser.LHOptimiserApp;
import com.rivigo.zoom.input.dto.InputHop;
import com.rivigo.zoom.model.ZRouteStop;
import com.rivigo.zoom.model.output.ZRouteFormat1;
import com.rivigo.zoom.utils.TimeUtils;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;

import java.util.*;

/**
 * Created by hgupta on 8/8/16.
 */
@Slf4j
@Getter
public class ZRoute {
    String routeCode;
    private List<ZRouteStop> stops = new ArrayList<ZRouteStop>();;

    public ZRoute(String routeCode) {
        this.routeCode = routeCode;
        String[] stopNameList = routeCode.split("-");
        for(int i=0; i<stopNameList.length; i++) {
            stops.add(new ZRouteStop(this, stopNameList[i], i));
        }
    }

    public ZRoute(InputHop h) {
        String routeName = h.startLoc + "-" + h.endLoc;
        this.routeCode = routeName;
        stops.add(new ZRouteStop(routeName, h.getStartLoc(), 0));
        stops.add(new ZRouteStop(routeName, h.getEndLoc(), 1));

    }

    public ZRouteStop getStartPoint() {
        Collections.sort(stops);
        return stops.get(0);
    }

    public ZRouteStop getEndPoint() {
        Collections.sort(stops);
        return stops.get(stops.size()-1);
    }

    public void addToEnd(ZRouteStop newStop) {
        newStop.setStopSequence(stops.size());
        stops.add(newStop);
        Collections.sort(stops);
    }

    public int getTouchPointCount() {
        return stops.size() - 2;
    }


    public long getTotalTat() {
        long totalTat = 0;
        for(int i=0; i<stops.size() -1; i++) {
            ZRouteStop start = stops.get(i);
            ZRouteStop end = stops.get(i + 1);
            totalTat += LHOptimiserApp.linehaulNetwork.getDirectTatMillis(start, end);
            totalTat += ZParams.VEHICLE_STOP_TIME_AT_PC_MINS  * TimeUtils.MILLIS_IN_MINUTE;
        }
        totalTat -= ZParams.VEHICLE_STOP_TIME_AT_PC_MINS  * TimeUtils.MILLIS_IN_MINUTE;
        return totalTat;
    }

    public static List<ZRouteFormat1> getOutputFormat1(Collection<ZRoute> routes) {
        List<ZRouteFormat1> retList = new ArrayList<ZRouteFormat1>();
        for(ZRoute r : routes) {
            retList.add(new ZRouteFormat1(r));
        }

        return retList;
    }

    public static Map<String, List<ZRoute>> getMap1(Collection<ZRoute> routes){
        Map<String, List<ZRoute>> map = new HashMap<String, List<ZRoute>>();
        for(ZRoute r : routes) {
            List<ZRouteStop>stops = r.getStops();
            for(int i=0; i<stops.size(); i++ ) {
                for(int j=i+1; j< stops.size(); j++) {
                    ZRouteStop start = stops.get(i);
                    ZRouteStop end = stops.get(j);
                    String code = start.getLocCode() + "-" + end.getLocCode();
                    List<ZRoute> rList = map.get(code);
                    if(rList == null) {
                        rList = new ArrayList<ZRoute>();
                        map.put(code, rList);
                    }
                    rList.add(r);
                }
            }
        }

        return map;
    }

    public long getDispatchTimeFromLoc(String loc) {
        if(loc.equals(getStartPoint().getLocCode())) {
            return 0l;
        }
        return getTatToLocation(loc) +  ZParams.VEHICLE_STOP_TIME_AT_PC_MINS  * TimeUtils.MILLIS_IN_MINUTE;
    }

    public long getTatToLocation(String loc) {
        return getTatBetweenLocations(stops.get(0).getLocCode(), loc);
    }

    public long getTatBetweenLocations(String srcPC, String destPC) {
        boolean startFound = false;
        boolean endFound = false;
        long totalTat = 0;

        if(srcPC.equals(destPC))
            return 0l;

        for(int i=0; i<stops.size() -1; i++) {
            ZRouteStop start = stops.get(i);
            ZRouteStop end = stops.get(i + 1);
            if(start.getLocCode().equals(srcPC)) {
                startFound = true;
            }
            if(startFound && !endFound) {
                totalTat += LHOptimiserApp.linehaulNetwork.getDirectTatMillis(start, end);
                totalTat += ZParams.VEHICLE_STOP_TIME_AT_PC_MINS  * TimeUtils.MILLIS_IN_MINUTE;
            }
            if(end.getLocCode().equals(destPC)) {
                endFound = true;
                break;
            }

        }
        totalTat -= ZParams.VEHICLE_STOP_TIME_AT_PC_MINS  * TimeUtils.MILLIS_IN_MINUTE;
        if(!startFound || !endFound) {
            log.error("Ref Route" + this);
            log.error("start: " + srcPC + ", end: " + destPC);
            throw new RuntimeException("Start and end point must lie in route");
        }
        return totalTat;
    }
}
