package com.rivigo.zoom.model.common.trip;

import com.rivigo.zoom.ZParams;
import com.rivigo.zoom.input.dto.TripScheduleInput;
import com.rivigo.zoom.model.ZRouteStop;
import com.rivigo.zoom.model.ZTripStop;
import com.rivigo.zoom.model.common.route.ZRoute;
import com.rivigo.zoom.LinehaulNetwork;
import com.rivigo.zoom.utils.TimeUtils;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hgupta on 11/8/16.
 */
@Getter
@Setter
public class ZTrip {
    private ZRoute route;
    private long dispatchTime;
//    private List<ZTripLeg> tripLegs;
    private List<ZTripStop> tripStops;

    public ZTrip(TripScheduleInput tripSchedule) {
        route = new ZRoute(tripSchedule.getClientRoute());
        dispatchTime = tripSchedule.getDispatchTime().getMillisOfDay();
    }

    public ZTrip(TripScheduleInput tripSchedule, LinehaulNetwork linehaulNetwork) {
        route = new ZRoute(tripSchedule.getClientRoute());
        dispatchTime = tripSchedule.getDispatchTime().getMillisOfDay();
        computeStops(linehaulNetwork);
    }

    public ZTrip(ZRoute r, long dispatchTime) {
        this.route = r;
        this.dispatchTime = dispatchTime;
//        createTripLegs();
    }

    public ZTrip(ZRoute r, long dispatchTime, LinehaulNetwork linehaulNetwork) {
        this.route = r;
        this.dispatchTime = dispatchTime;
//        createTripLegs();
        computeStops(linehaulNetwork);
    }


    public void computeStops(LinehaulNetwork linehaulNetwork) {
        this.tripStops = new ArrayList<ZTripStop>();

        List<ZRouteStop> rStopList = route.getStops();
        long prevDepartureTime = dispatchTime;
        for(int i =0; i<rStopList.size(); i++) {
            ZRouteStop currRouteStop = rStopList.get(i);
            ZTripStop tripStop = new ZTripStop(i, currRouteStop.getLocCode(), this);
            if(i == 0) {
                tripStop.setDepartureTime(this.dispatchTime);
            } else {
                ZRouteStop prevRouteStop = rStopList.get(i-1);
                long transitTime = linehaulNetwork.getDirectTatMillis(prevRouteStop.getLocCode(), currRouteStop.getLocCode());
                tripStop.setArrivalTime(prevDepartureTime + transitTime);
                tripStop.setDepartureTime(tripStop.getArrivalTime() + ZParams.VEHICLE_STOP_TIME_AT_PC_MINS* TimeUtils.MILLIS_IN_MINUTE);
            }
            prevDepartureTime = tripStop.getDepartureTime();
            this.tripStops.add(tripStop);
        }
    }

    public String getTripCode() {
        return this.route.getRouteCode();
    }
}
