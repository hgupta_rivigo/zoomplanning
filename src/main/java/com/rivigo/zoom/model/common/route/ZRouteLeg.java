package com.rivigo.zoom.model.common.route;

import com.rivigo.zoom.model.ZRouteStop;

/**
 * Created by hgupta on 8/8/16.
 */
public class ZRouteLeg {
    public ZRouteStop src;
    public ZRouteStop dest;
    public long tatMillis;
}
