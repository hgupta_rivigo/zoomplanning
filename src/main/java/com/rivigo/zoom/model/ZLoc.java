package com.rivigo.zoom.model;

import com.rivigo.zoom.ZParams;
import com.rivigo.zoom.input.dto.LocationInput;
import com.rivigo.zoom.utils.TimeUtils;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

/**
 * Created by hgupta on 8/8/16.
 */
@Getter
@Setter
public class ZLoc {
    protected String loc;
    protected long holingTime = ZParams.TRANSHIPMENT_BUFFER_MINS * TimeUtils.MILLIS_IN_MINUTE;
    protected boolean isRouteEndPoint = true;

    public ZLoc(String loc) {
        this.loc = loc;
    }
    public ZLoc(LocationInput locIn) {
        this.loc = locIn.getLocationCode();
        this.holingTime = locIn.getHoldingTimeHrs() * TimeUtils.MILLIS_IN_HOUR;
        this.isRouteEndPoint = locIn.getIsEndPoint() == 1;
    }

    @Override
    public String toString() {
        return loc;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        ZLoc zLoc = (ZLoc) o;

        return new EqualsBuilder()
                .append(loc, zLoc.loc)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(loc)
                .toHashCode();
    }
}
