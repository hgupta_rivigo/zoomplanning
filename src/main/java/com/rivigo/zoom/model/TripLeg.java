package com.rivigo.zoom.model;

import com.rivigo.zoom.utils.TimeUtils;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hgupta on 29/7/16.
 */
@Getter
@Setter
public class TripLeg implements Comparable<TripLeg> {
    public int startDayOfTrip;
    public String routeCode;
    public Integer tripScheduleId;

    public String srcLoc;
    public String destLoc;

    public long startTimeMillis;
    public long travelTimeMillis;

    public List<TripLeg> nextPossibleLegs;

    public TripLeg() {

    }

    public String getStartTime() {
        int dayCount = (int) (startTimeMillis / TimeUtils.MILLIS_IN_DAY);
        long millis = startTimeMillis % TimeUtils.MILLIS_IN_DAY;
        int hourOfDay = (int) (millis/TimeUtils.MILLIS_IN_HOUR);
        millis = millis%TimeUtils.MILLIS_IN_HOUR;
        int minsOfHour = (int) (millis/(60 * 1000l));

        return "Day_" + dayCount + " " + hourOfDay + ":" + minsOfHour;
    }

    public String getTat() {
       int tat = (int) (travelTimeMillis/TimeUtils.MILLIS_IN_HOUR);
       return tat + "hours";
    }

    public long getEndTimeMillis() {
        return this.startTimeMillis + this.travelTimeMillis;
    }

    public String getEndTime() {
        long millis = startTimeMillis + travelTimeMillis;
        int dayCount = (int) (millis / TimeUtils.MILLIS_IN_DAY);
        millis = millis % TimeUtils.MILLIS_IN_DAY;
        int hourOfDay = (int) (millis/TimeUtils.MILLIS_IN_HOUR);
        millis = millis%TimeUtils.MILLIS_IN_HOUR;
        int minsOfHour = (int) (millis/(60 * 1000l));

        return "Day_" + dayCount + " " + hourOfDay + ":" + minsOfHour;
    }

    public void addNextLeg(TripLeg leg) {
        if(this.nextPossibleLegs == null) {
            this.nextPossibleLegs = new ArrayList();
        }
        this.nextPossibleLegs.add(leg);
    }

    public TripLeg clone() {
        TripLeg rl = new TripLeg();
        rl.startDayOfTrip = startDayOfTrip;
        rl.routeCode = routeCode;
        rl.tripScheduleId = tripScheduleId;

        rl.srcLoc = srcLoc;
        rl.destLoc = destLoc;

        rl.startTimeMillis = startTimeMillis;
        rl.travelTimeMillis = travelTimeMillis;

        return rl;
    }


    public int compareTo(TripLeg tripLeg) {
        return (int) (this.startTimeMillis - tripLeg.startTimeMillis);
    }

    public String getTripId() {
        return "Day_" + startDayOfTrip + ":" + routeCode + ":" + tripScheduleId;
    }
}
