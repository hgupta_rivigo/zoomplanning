package com.rivigo.zoom.model;

import com.rivigo.zoom.ZCache;
import com.rivigo.zoom.model.common.route.ZRoute;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by hgupta on 29/7/16.
 */
@Getter
@Setter
public class ZRouteStop implements Comparable<ZRouteStop> {
    protected ZRoute route;
    protected int stopSequence;
    protected ZLoc zLoc;

    public String getLocCode() {
        return zLoc.getLoc();
    }

    public ZRouteStop(ZRoute route,  String location, int stopSequence) {
        this.route = route;
        this.stopSequence = stopSequence;
        zLoc = ZCache.getZLoc(location);
    }
    public ZRouteStop(String routeName,  String location, int stopSequence) {
        this.route = ZCache.getRoute(routeName);
        this.stopSequence = stopSequence;
        zLoc = ZCache.getZLoc(location);
    }

    public int compareTo(ZRouteStop routeStop) {
        if(this.route.equals(routeStop.getRoute()) == false)
            throw new RuntimeException();

        return this.stopSequence - routeStop.stopSequence;
    }

    @Override
    public String toString() {
        return this.route + "#" + stopSequence + ": " + this.zLoc;
    }
}
