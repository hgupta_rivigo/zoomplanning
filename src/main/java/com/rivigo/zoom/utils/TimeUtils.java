package com.rivigo.zoom.utils;

import org.joda.time.LocalTime;

/**
 * Created by hgupta on 3/8/16.
 */
public class TimeUtils {
    private TimeUtils() {
    }

    public static final long MILLIS_IN_DAY = 24 * 3600 * 1000l;
    public static final long MILLIS_IN_HOUR = 3600 * 1000l;
    public static final long MILLIS_IN_MINUTE = 60 * 1000l;

    public static LocalTime getLocalTime(long millis) {

        millis = (((millis % MILLIS_IN_DAY) + MILLIS_IN_DAY) % MILLIS_IN_DAY);
        int hrs = (int) (millis / TimeUtils.MILLIS_IN_HOUR);
        millis %= TimeUtils.MILLIS_IN_HOUR;

        int mins = (int) (millis / TimeUtils.MILLIS_IN_MINUTE);
        millis %= MILLIS_IN_MINUTE;

        int secs = (int) (millis/1000l);
        millis %= 1000l;

        return new LocalTime(hrs, mins, secs, (int) millis);
    }
}
