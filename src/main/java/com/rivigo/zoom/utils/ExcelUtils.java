package com.rivigo.zoom.utils;

import com.rivigo.common.report.impl.GenericReportReaderImpl;
import com.rivigo.zoom.annotations.NoSpaceString;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

/**
 * Created by hgupta on 3/8/16.
 */
@Slf4j
public class ExcelUtils {
    private ExcelUtils() {}

    public static <T> List<T> readFromSheet(Workbook wb, String sheetName, Class<T> clazz)
            throws IOException, InvalidFormatException, IllegalAccessException {
        Sheet sh = wb.getSheet(sheetName);
        Collection c = GenericReportReaderImpl.read(sh, clazz);
        List<T> cList = new ArrayList(c);

        int emptyObject = 0;
        if(cList != null) {
            Iterator<T> it = cList.iterator();
            while(it.hasNext()) {
                T obj = it.next();
                boolean hasNonNull = false;
                for (Field field : obj.getClass().getDeclaredFields()) {
                    //field.setAccessible(true); // if you want to modify private fields
                    if(field.isAnnotationPresent(NoSpaceString.class)
                            && field.getType() == String.class) {
                        String value = (String) field.get(obj);
                        if(value != null) {
                            field.set(obj, value.replaceAll(" ", ""));
                        }

                    }
                    if(field.get(obj) != null)
                        hasNonNull = true;
                }

                if(!hasNonNull) {
                    emptyObject++;
                    it.remove();
                }
            }
        }
        log.info("Returning {} instances of {} after rejecting {} empty row", cList.size(), clazz.getSimpleName(), emptyObject);
        return cList;
    }

    public static void writeToFile(SXSSFWorkbook wb, String fileName) throws IOException {
        File f = new File("target");
        if (f.exists() && f.isDirectory()) {
            fileName = "target/" + fileName;
        }

        DateTime now = new DateTime(DateTimeZone.forOffsetHoursMinutes(5, 30));
        DateTimeFormatter dateAndTimeFormatter = DateTimeFormat.forPattern("dd_MMM_HH_mm_ss");
        String time = dateAndTimeFormatter.print(now);
        fileName += time;

        if(!fileName.endsWith(".xlsx")) {
            fileName += ".xlsx";
        }
        FileOutputStream fileOutputStream = new FileOutputStream(fileName);
        wb.write(fileOutputStream);
        fileOutputStream.close();
    }
}
