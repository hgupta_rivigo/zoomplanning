package com.rivigo.zoom;

import com.rivigo.zoom.model.ZLoc;
import com.rivigo.zoom.model.common.route.ZRoute;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by hgupta on 9/8/16.
 */

public class ZCache {
    private static Map<String, ZRoute> routeMap = new HashMap<String, ZRoute>();
    private static Map<String, ZLoc> locMap = new HashMap<String, ZLoc>();

    public static ZLoc getZLoc(String locCode) {
        ZLoc ret = locMap.get(locCode);
        if(ret == null) {
            ret = new ZLoc(locCode);
            locMap.put(locCode, ret);
        }

        return ret;
    }

    public static ZRoute getRoute(String routeName) {
        ZRoute ret = routeMap.get(routeName);
        if(ret == null) {
            ret = new ZRoute(routeName);
            routeMap.put(routeName, ret);
        }

        return ret;
    }
}
