package com.rivigo.zoom.datatransformation;

import com.rivigo.common.report.impl.GenericReportGeneratorImpl;
import com.rivigo.zoom.ZParams;
import com.rivigo.zoom.ZSheetName;
import com.rivigo.zoom.datatransformation.algo.PickupDeliveryAnalyser;
import com.rivigo.zoom.input.dto.TripScheduleInput;
import com.rivigo.zoom.input.dto.ZParam;
import com.rivigo.zoom.input.reader.ClientPromisedTatReader;
import com.rivigo.zoom.model.*;
import com.rivigo.zoom.input.dto.ClientPromisedTat;
import com.rivigo.zoom.input.dto.InputHop;
import com.rivigo.zoom.model.output.RouteLegFormat;
import com.rivigo.zoom.LinehaulNetwork;
import com.rivigo.zoom.utils.ExcelUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

import java.io.File;
import java.util.*;

/**
 * Created by hitesh on 3/31/16.
 */
@Slf4j
public class PickupDeliveryAnalyserApp {

    public static final String INPUT_FILE = ZSheetName.INPUT_WORKBOOK;
    public static final String OUTPUT_FILE = "output_";

    public static LinehaulNetwork linehaulNetwork;


    public static void main(String[] args) throws Exception {
        SXSSFWorkbook wb = new SXSSFWorkbook();

        File f = new File(INPUT_FILE);
        Workbook inputWB = WorkbookFactory.create(f);

        List<ZParam> params = ExcelUtils.readFromSheet(inputWB, ZSheetName.INPUT_ZPARAMS, ZParam.class);
        ZParams.initialize(params);

        List<InputHop> tats = ExcelUtils.readFromSheet(inputWB, ZSheetName.INPUT_TATS, InputHop.class);
        linehaulNetwork = new LinehaulNetwork(tats);

        List<ClientPromisedTat> cpts = ClientPromisedTatReader.readExcel(inputWB.getSheet(ZSheetName.INPUT_CLIENT_PROMISED_TAT));
        cpts = ClientPromisedTatReader.removeSameSrcDest(cpts);

        List<TripScheduleInput> tripSchedules = ExcelUtils.readFromSheet(inputWB, ZSheetName.INPUT_TRIP_SCHEDULES, TripScheduleInput.class);
        validateSchedules(tripSchedules, linehaulNetwork);

        GenericReportGeneratorImpl.write(wb.createSheet(ZSheetName.OUTPUT_CLIENT_PROMISED_TAT), ClientPromisedTat.class, cpts);
        GenericReportGeneratorImpl.write(wb.createSheet(ZSheetName.OUTPUT_TRIP_SCHEDULES), TripScheduleInput.class, tripSchedules);


        PickupDeliveryAnalyser.run(cpts, tripSchedules, linehaulNetwork, wb);
        ExcelUtils.writeToFile(wb, OUTPUT_FILE);

    }

    private static void validateSchedules(List<TripScheduleInput> tripSchedules, LinehaulNetwork linehaulNetwork) {
        StringBuilder sb = new StringBuilder();
        for(TripScheduleInput ts: tripSchedules) {
            String[] stopList = ts.getClientRoute().split("-");
            for(int i=0; i<stopList.length-1; i++) {
                String startStop = stopList[i];
                String endStop = stopList[i + 1];
                Long directTat = linehaulNetwork.getDirectTatMillis(startStop, endStop);
                if(directTat == null) {
                    sb.append("\nTat not present: " + startStop + "\t to \t" + endStop);
                }
            }
        }
        if(sb.length() > 0) {
            log.error("Following tats were not found : " + sb.toString());
            throw new RuntimeException("Tats missing. Look previous line");
        }
    }
}
