package com.rivigo.zoom.datatransformation.algo;

import com.rivigo.zoom.ZParams;
import com.rivigo.zoom.input.dto.ClientPromisedTat;
import com.rivigo.zoom.model.TripLeg;
import com.rivigo.zoom.utils.TimeUtils;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.Map;

/**
 * Created by hgupta on 10/8/16.
 */
@Slf4j
public class WorkerThread implements Runnable{
    Map<ClientPromisedTat, List<List<TripLeg>>> solutionMap;
    LinehaulDAG_v2 graph;
    ClientPromisedTat cpt;
    public WorkerThread(Map<ClientPromisedTat, List<List<TripLeg>>> map,LinehaulDAG_v2 graph, ClientPromisedTat cpt) {
        solutionMap = map;
        this.graph = graph;
        this.cpt = cpt;
    }
    public void run() {
        log.info("Processing CPT: " + cpt);
        if(cpt.getSrcPC().equals(cpt.getDestPC()))
            return;
        List<List<TripLeg>> solution = null;
        long pickupTimeWithBuffer = cpt.getPickupTimeMillis() + ZParams.MIN_PICKUP_BUFFER_MINS  * TimeUtils.MILLIS_IN_MINUTE;
        long deliveryTimeWithBuffer = cpt.getDeliveryTimeAbsolute() - ZParams.MIN_DELIVERY_BUFFER_MINS * TimeUtils.MILLIS_IN_MINUTE;

        int attempt = 0;
        while((solution == null || solution.isEmpty()) && attempt < 8) {
            solution = graph.findPaths(cpt.getSrcPC(), cpt.getDestPC(),
                    pickupTimeWithBuffer,
                    deliveryTimeWithBuffer + attempt * 6 * TimeUtils.MILLIS_IN_HOUR, ZParams.MAX_TRANSHIPMENTS + 1 );
            log.debug("Attempt" + attempt);
            attempt++;
        }
        PickupDeliveryAnalyser.filterSolutions(solution);
        solutionMap.put(cpt, solution);
    }
}
