package com.rivigo.zoom.datatransformation.algo;

import com.rivigo.common.report.impl.GenericReportGeneratorImpl;
import com.rivigo.zoom.ZParams;
import com.rivigo.zoom.model.TripLeg;
import com.rivigo.zoom.input.dto.TripScheduleInput;
import com.rivigo.zoom.input.dto.ClientPromisedTat;
import com.rivigo.zoom.model.output.PickupDeliveryAnalyserFormat1;
import com.rivigo.zoom.LinehaulNetwork;
import com.rivigo.zoom.utils.TimeUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by hgupta on 10/8/16.
 */
@Slf4j
public class PickupDeliveryAnalyser {
    public static void run(List<ClientPromisedTat> cpts,
                    List<TripScheduleInput> tripSchedules,
                    LinehaulNetwork linehaulNetwork,
                    SXSSFWorkbook outputWB) {
        List<TripLeg> tripLegs = getRouteLegs(tripSchedules, linehaulNetwork);

        LinehaulDAG_v2 graph = new LinehaulDAG_v2(tripLegs);
        Map<ClientPromisedTat, List<List<TripLeg>>> solutionMap =
                Collections.synchronizedMap(new HashMap<ClientPromisedTat, List<List<TripLeg>>>());

        ExecutorService executor = Executors.newFixedThreadPool(4);

        int solutionCount  = 0;
        for(ClientPromisedTat cpt: cpts) {
            WorkerThread wt = new WorkerThread(solutionMap, graph, cpt);
            executor.execute(wt);
        }

        executor.shutdown();
        while (!executor.isTerminated()) {
            log.info("===============");
            try {
                Thread.sleep(10000l);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        publishOutput(solutionMap, outputWB);
    }

    public static List<TripLeg> getRouteLegs(List<TripScheduleInput> tripSchedules,
                                              LinehaulNetwork linehaulNetwork) {

        List<TripLeg> allDayZeroTripLegs = new ArrayList();
        for(TripScheduleInput ts: tripSchedules) {
            log.debug(ts.getClientRoute());
            String[] stopList = ts.getClientRoute().split("-");
            long startTime = ts.getDispatchTime().getMillisOfDay();
            List<TripLeg> dayZeroTripLegs = new ArrayList<TripLeg>();
            for(int i=0; i<stopList.length-1; i++) {
                String startStop = stopList[i];
                String endStop = stopList[i+1];
                TripLeg leg = new TripLeg();
                leg.startDayOfTrip = 0;
                leg.routeCode = ts.getClientRoute();
                leg.srcLoc = startStop;
                leg.destLoc = endStop;
                leg.startTimeMillis = startTime;
                leg.travelTimeMillis = linehaulNetwork.getDirectTatMillis(startStop, endStop);
                leg.tripScheduleId = ts.getScheduleId();
                dayZeroTripLegs.add(leg);

                startTime += linehaulNetwork.getDirectTatMillis(startStop, endStop);
                startTime += ZParams.VEHICLE_STOP_TIME_AT_PC_MINS * TimeUtils.MILLIS_IN_MINUTE;
            }
            allDayZeroTripLegs.addAll(dayZeroTripLegs);
        }

        List<TripLeg> legsForNextAndLastFewDays = new ArrayList();
        for(int i = -3; i<=10; i++) {
            for(TripLeg rl : allDayZeroTripLegs) {
                long legStartTime = rl.getStartTimeMillis() + i * TimeUtils.MILLIS_IN_DAY;
                if(legStartTime < 0)
                    continue;

                TripLeg newRl = rl.clone();
                newRl.startTimeMillis = legStartTime;
                newRl.startDayOfTrip = i;
                legsForNextAndLastFewDays.add(newRl);
            }
        }

        Collections.sort(legsForNextAndLastFewDays);

        return legsForNextAndLastFewDays;
    }

    private static void publishOutput(Map<ClientPromisedTat, List<List<TripLeg>>> solutionMap,
                                      SXSSFWorkbook wb) {

        List<PickupDeliveryAnalyserFormat1> outputSheet1 = new ArrayList();
        List<PickupDeliveryAnalyserFormat1> outputSheet2 = new ArrayList();
        for(ClientPromisedTat cpt2: solutionMap.keySet()) {
            log.info("Processing " + cpt2);
            List<List<TripLeg>> solutionList = solutionMap.get(cpt2);
            if(solutionList == null || solutionList.isEmpty()) {
                PickupDeliveryAnalyserFormat1 out = new PickupDeliveryAnalyserFormat1(cpt2);
                outputSheet1.add(out);
                outputSheet2.add(out);
            } else {
                int option = 1;
                PickupDeliveryAnalyserFormat1 bestOutput = null;
                for(List<TripLeg> rlList : solutionList) {
                    String optionStr = "option " + option++ + "/" + solutionList.size();
                    PickupDeliveryAnalyserFormat1 out = new PickupDeliveryAnalyserFormat1(cpt2, rlList, optionStr);
                    outputSheet1.add(out);
                    if(bestOutput == null || bestOutput.getDeliveryBuffer() < out.getDeliveryBuffer()) {
                        bestOutput = out;
                    }
                }
                outputSheet2.add(bestOutput);
            }
        }

        Collections.sort(outputSheet1);
        Collections.sort(outputSheet2);

        GenericReportGeneratorImpl.write(wb.createSheet("out_all"), PickupDeliveryAnalyserFormat1.class, outputSheet1);
        GenericReportGeneratorImpl.write(wb.createSheet("out_best"), PickupDeliveryAnalyserFormat1.class, outputSheet2);
    }

    private static void removeIdenticalSolutions(List<List<TripLeg>> solutions) {
        Set<String> solutionString = new HashSet();
        Iterator<List<TripLeg>> it = solutions.iterator();
        while(it.hasNext()) {
            List<TripLeg> sol = it.next();
            String hash = "";
            for(TripLeg rl : sol) {
                hash += rl.getRouteCode();
            }

            if(solutionString.contains(hash)) {
                it.remove();
            }

            solutionString.add(hash);
        }
    }
    public static void filterSolutions(List<List<TripLeg>> cptSolutions) {
            if(cptSolutions == null) {
                return;
            }

            removeIdenticalSolutions(cptSolutions);

            if(cptSolutions.size() <= 1) {
                return;
            }

            boolean hasDirectRoute = false;
            for(List<TripLeg> solution: cptSolutions) {
                if(solution.size() == 1) {
                    hasDirectRoute = true;
                    break;
                }
            }

            Iterator<List<TripLeg>> iterator = cptSolutions.iterator();
            while(iterator.hasNext()) {
                List<TripLeg> solution = iterator.next();
                if(solution.size() > ZParams.MAX_TRANSHIPMENTS + 1) {
                    iterator.remove();
                } else if(hasDirectRoute && solution.size() > 1) {
                    iterator.remove();
                }
            }
        }


}
