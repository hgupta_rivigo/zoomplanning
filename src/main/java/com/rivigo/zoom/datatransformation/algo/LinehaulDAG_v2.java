package com.rivigo.zoom.datatransformation.algo;

import com.rivigo.zoom.ZParams;
import com.rivigo.zoom.model.TripLeg;
import com.rivigo.zoom.utils.TimeUtils;

import java.util.*;

public class LinehaulDAG_v2 {

    public Map<String, List<TripLeg> > outgoingLegsMap;
    public Map<String, List<TripLeg> > incomingLegsMap;

    public LinehaulDAG_v2(List<TripLeg> legs) {
        this.outgoingLegsMap = new HashMap();
        this.incomingLegsMap = new HashMap();
        List<TripLeg> allLegs = generateAllLegs(legs);
        buildGraph(allLegs);
    }

    private List<TripLeg> generateAllLegs(List<TripLeg> legs) {
        Map<String, List<TripLeg>> tripToTripLegMapping = new HashMap<String, List<TripLeg>>();
        for(TripLeg tl : legs) {
            List<TripLeg> tripLegs = tripToTripLegMapping.get(tl.getTripId());
            if(tripLegs == null) {
                tripLegs = new ArrayList<TripLeg>();
                tripToTripLegMapping.put(tl.getTripId(), tripLegs);
            }
            tripLegs.add(tl);
        }

        List<TripLeg> retList = new ArrayList<TripLeg>();

        for(List<TripLeg> tripLegs : tripToTripLegMapping.values()) {
            Collections.sort(tripLegs);
            for(int i=0; i<tripLegs.size(); i++) {
                for(int j=i; j<tripLegs.size(); j++) {
                    if(j == i) {
                        retList.add(tripLegs.get(i));
                    } else {
                        TripLeg leg1 = tripLegs.get(i);
                        TripLeg leg2 = tripLegs.get(j);
                        TripLeg mergedLeg = new TripLeg();
                        mergedLeg.startDayOfTrip = leg1.startDayOfTrip;
                        mergedLeg.routeCode = leg1.routeCode;
                        mergedLeg.srcLoc = leg1.srcLoc;
                        mergedLeg.destLoc = leg2.destLoc;
                        mergedLeg.startTimeMillis = leg1.startTimeMillis;
                        mergedLeg.travelTimeMillis = leg2.getEndTimeMillis() - leg1.getStartTimeMillis();
                        mergedLeg.tripScheduleId = leg1.tripScheduleId;

                        retList.add(mergedLeg);
                    }
                }
            }
        }

        return retList;
    }


    public List<List<TripLeg>> findPaths(String startPoint, String endPoint,
                                        long startTimeMillis, long endTimeMillis,
                                         int maxLegs) {
        List<List<TripLeg>> solutionList = new ArrayList<List<TripLeg>>();

        if(outgoingLegsMap.get(startPoint) == null || outgoingLegsMap.get(startPoint).isEmpty())
            return solutionList;

        Queue<List<TripLeg>> queue = new LinkedList<List<TripLeg>>();
        for(TripLeg tl : outgoingLegsMap.get(startPoint)) {
            if(tl.getStartTimeMillis() > startTimeMillis) {
                ArrayList<TripLeg> path = new ArrayList<TripLeg>();
                path.add(tl);
                queue.add(path);
            }
        }

        int lastSolutionLegCount = 0;
        while(!queue.isEmpty()) {
            List<TripLeg> solutionLegs = queue.remove();
            if(isValidSolution(solutionLegs, startPoint, endPoint, startTimeMillis, endTimeMillis, maxLegs)) {
                solutionList.add(solutionLegs);
                if(lastSolutionLegCount > 0 && lastSolutionLegCount < solutionLegs.size()) {
                    break;
                }
            } else {
                //if valid for continuation, add multiple new entries to queue
                if(solutionLegs.size() >= maxLegs)
                    continue;

                TripLeg lastLeg = solutionLegs.get(solutionLegs.size() -1);
                if(lastLeg.getEndTimeMillis() > endTimeMillis)
                    continue;

                if(lastLeg.getNextPossibleLegs() == null || lastLeg.getNextPossibleLegs().isEmpty())
                    continue;

                Set<String> locs = new HashSet<String>();
                for(TripLeg tl : solutionLegs) {
                    locs.add(tl.getSrcLoc());
                    locs.add(tl.getDestLoc());
                }

                for(TripLeg nextLeg : lastLeg.getNextPossibleLegs()) {
                    if(!locs.contains(nextLeg.getDestLoc())) {
                        List<TripLeg> nextPossibility = new ArrayList<TripLeg>(solutionLegs);
                        nextPossibility.add(nextLeg);
                        queue.add(nextPossibility);
                    }
                }

            }
        }

        return solutionList;
    }

    private boolean isValidSolution(List<TripLeg> solutionLegs,
                                    String startPoint, String endPoint,
                                    long startTimeMillis, long endTimeMillis,
                                    int maxLegs) {
        if(solutionLegs.size() > maxLegs) {
            return false;
        }

        TripLeg startLeg = solutionLegs.get(0);
        if(startLeg.getSrcLoc().equals(startPoint) == false)
            return  false;

        if(startLeg.getStartTimeMillis() < startTimeMillis)
            return false;

        TripLeg endLeg = solutionLegs.get(solutionLegs.size() - 1);
        if(endLeg.getDestLoc().equals(endPoint) == false)
            return  false;

        if(endLeg.getEndTimeMillis() > endTimeMillis)
            return false;

        return true;
    }

    public void buildGraph(List<TripLeg> tripLegs) {

        //build outgoing and incoming trip list
        outgoingLegsMap.clear();
        incomingLegsMap.clear();

        Collections.sort(tripLegs);

        for(TripLeg currLeg : tripLegs) {
            //add this leg to outgoing legs
            List<TripLeg> legsStartingAtStartPoint = outgoingLegsMap.get(currLeg.srcLoc);
            if (legsStartingAtStartPoint == null) {
                legsStartingAtStartPoint = new ArrayList();
                outgoingLegsMap.put(currLeg.srcLoc, legsStartingAtStartPoint);
            }
            legsStartingAtStartPoint.add(currLeg);

            //add this leg to incoming legs
            List<TripLeg> legsEndingAtEndPoint = incomingLegsMap.get(currLeg.destLoc);
            if (legsEndingAtEndPoint == null) {
                legsEndingAtEndPoint = new ArrayList();
                incomingLegsMap.put(currLeg.destLoc, legsEndingAtEndPoint);
            }
            legsEndingAtEndPoint.add(currLeg);
        }

        //let's start building directed graph
        for(TripLeg currLeg : tripLegs) {

            List<TripLeg> legsEndingAtStartPoint = incomingLegsMap.get(currLeg.srcLoc);
            if(legsEndingAtStartPoint == null) {
                continue;
            }
            for(TripLeg prevLeg : legsEndingAtStartPoint) {
                if(prevLeg.getTripId().equals(currLeg.getTripId()) == false
                   && prevLeg.getEndTimeMillis() + ZParams.TRANSHIPMENT_BUFFER_MINS * TimeUtils.MILLIS_IN_MINUTE <= currLeg.startTimeMillis
                   && prevLeg.getEndTimeMillis() + ZParams.TRANSHIPMENT_BUFFER_MINS * TimeUtils.MILLIS_IN_MINUTE + TimeUtils.MILLIS_IN_DAY > currLeg.startTimeMillis ) {
                    prevLeg.addNextLeg(currLeg);
                }
            }
        }
    }
}
