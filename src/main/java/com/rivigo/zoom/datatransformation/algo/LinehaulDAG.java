package com.rivigo.zoom.datatransformation.algo;

import com.rivigo.zoom.model.TripLeg;
import com.rivigo.zoom.utils.TimeUtils;

import java.util.*;

public class LinehaulDAG  {

    public Map<String, List<TripLeg> > outgoingLegsMap;
    public Map<String, List<TripLeg> > incomingLegsMap;

    public static final long TRANS_SHIPMENT_BUFFER = 0 * 3600 * 1000l;

    public LinehaulDAG(List<TripLeg> legs) {
        this.outgoingLegsMap = new HashMap();
        this.incomingLegsMap = new HashMap();
        buildGraph(legs);
    }

    public List<List<TripLeg>> findLegs(String startPoint, String endPoint,
                                        long startTimeMillis, long endTimeMillis) {

        List<List<TripLeg>> retList = new ArrayList();

        List<TripLeg> outgoingLegs = outgoingLegsMap.get(startPoint);
        if(outgoingLegs == null) {
            return retList;
        }
        for(TripLeg startLeg : outgoingLegs) {
            if(/*startLeg != null && startLeg.startTime != null &&*/
                    startLeg.startTimeMillis > startTimeMillis) {
                LinkedList<TripLeg> directPath = findDirectPath(startLeg, endPoint, endTimeMillis);
                if(directPath != null && !directPath.isEmpty()) {
                    retList.add(directPath);
                } else {
                    List<LinkedList<TripLeg>> paths = findLegs(startLeg, endPoint, endTimeMillis);
                    if (paths != null) {
                        retList.addAll(paths);
                    }
                }
            }
        }

        return retList;

    }

    private LinkedList<TripLeg> findDirectPath(TripLeg startLeg, String endPoint, long endTimeMillis) {
//        LinkedList<TripLeg> path = new LinkedList<TripLeg>();
//        TripLeg lastLeg = startLeg;
//        path.add(lastLeg);
//        while(lastLeg.nextLegSameTrip != null) {
//            if(lastLeg.getDestLoc().equals(endPoint)) {
//                break;
//            }
//            lastLeg = lastLeg.nextLegSameTrip;
//            path.add(lastLeg);
//        }
//
//        lastLeg = path.getLast();
//        if(lastLeg.getDestLoc().equals(endPoint) && lastLeg.getEndTimeMillis() < endTimeMillis) {
//            mergeAdjacentLegs(path);
//            return path;
//        }

        return null;
    }

    /**
     * LinkedList because we want addFirstMethod too.
     *
     * Returns list of paths starting with startLeg AND ending on endPoint before endTime.
     * The list of paths will include startLeg.
     *
     * Return null if no path can be found.
     * Return Path if atleast one path can be found.
     */
    public List<LinkedList<TripLeg>> findLegs(TripLeg startLeg, String endPoint, long endTime) {
        if(startLeg.startTimeMillis + startLeg.travelTimeMillis > endTime) {
            return null;
        }

        List<LinkedList<TripLeg>> retList = new ArrayList();

        //base case:
        if(startLeg.destLoc.equals(endPoint)) {
            LinkedList<TripLeg> path = new LinkedList();
            path.add(startLeg);
            retList.add(path);
            return retList;
        }

        if (startLeg.nextPossibleLegs == null) {
            return null;
        }

        for(TripLeg t: startLeg.nextPossibleLegs) {
            List<LinkedList<TripLeg>> nextPaths = findLegs(t, endPoint, endTime);
            if(nextPaths == null || nextPaths.isEmpty()) {
                continue;
            }
            for(LinkedList<TripLeg> path : nextPaths) {
                path.addFirst(startLeg);
                retList.add(path);
            }
        }

        if(retList.isEmpty() == false) {
            for(List<TripLeg> legs: retList) {
                mergeAdjacentLegs(legs);
            }
            return retList;
        }
        return null;
    }

    public void buildGraph(List<TripLeg> tripLegs) {
        long starttime = System.currentTimeMillis();

        //build outgoing and incoming trip list
        outgoingLegsMap.clear();
        incomingLegsMap.clear();

        Collections.sort(tripLegs);

        for(TripLeg currLeg : tripLegs) {
            //add this leg to outgoing legs
            List<TripLeg> legsStartingAtStartPoint = outgoingLegsMap.get(currLeg.srcLoc);
            if (legsStartingAtStartPoint == null) {
                legsStartingAtStartPoint = new ArrayList();
                outgoingLegsMap.put(currLeg.srcLoc, legsStartingAtStartPoint);
            }
            legsStartingAtStartPoint.add(currLeg);

            //add this leg to incoming legs
            List<TripLeg> legsEndingAtEndPoint = incomingLegsMap.get(currLeg.destLoc);
            if (legsEndingAtEndPoint == null) {
                legsEndingAtEndPoint = new ArrayList();
                incomingLegsMap.put(currLeg.destLoc, legsEndingAtEndPoint);
            }
            legsEndingAtEndPoint.add(currLeg);
        }

        //let's start building directed graph
        for(TripLeg currLeg : tripLegs) {

            List<TripLeg> legsEndingAtStartPoint = incomingLegsMap.get(currLeg.srcLoc);
            if(legsEndingAtStartPoint == null) {
                continue;
            }
            for(TripLeg prevLeg : legsEndingAtStartPoint) {
                if(prevLeg.getTripId().equals(currLeg.getTripId())
                   || (prevLeg.startTimeMillis + prevLeg.travelTimeMillis + TRANS_SHIPMENT_BUFFER <= currLeg.startTimeMillis
                        && prevLeg.startTimeMillis + prevLeg.travelTimeMillis
                        + TRANS_SHIPMENT_BUFFER + TimeUtils.MILLIS_IN_DAY > currLeg.startTimeMillis )) {
                    prevLeg.addNextLeg(currLeg);
                }
            }
        }
    }

    public void mergeAdjacentLegs(List<TripLeg> legs) {
        for(int i=0; i<legs.size() - 1;) {
            TripLeg mergedLeg = mergeLegs(legs.get(i), legs.get(i+1));
            if(mergedLeg == null) {
                i++;
            } else {
                legs.remove(i+1);
                legs.remove(i);
                legs.add(i, mergedLeg);
            }
        }
    }

    public TripLeg mergeLegs(TripLeg leg1, TripLeg leg2) {
        if(!leg1.getTripId().equals(leg2.getTripId())) {
            //legs dont belong to same trip
            return null;
        }

        if(leg1.destLoc.equals(leg2.srcLoc) == false) {
            //legs are not consecutive
            return null;
        }
        TripLeg mergedLeg = new TripLeg();
        mergedLeg.startDayOfTrip = leg1.startDayOfTrip;
        mergedLeg.routeCode = leg1.routeCode;
        mergedLeg.srcLoc = leg1.srcLoc;
        mergedLeg.startTimeMillis = leg1.startTimeMillis;
        mergedLeg.destLoc = leg2.destLoc;
        mergedLeg.travelTimeMillis = leg1.travelTimeMillis + leg2.travelTimeMillis;
        mergedLeg.nextPossibleLegs = leg2.nextPossibleLegs;
        mergedLeg.tripScheduleId = leg1.tripScheduleId;

        return mergedLeg;
    }

}
