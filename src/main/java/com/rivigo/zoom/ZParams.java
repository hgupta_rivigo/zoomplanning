package com.rivigo.zoom;

import com.rivigo.zoom.input.dto.ZParam;
import com.rivigo.zoom.utils.TimeUtils;

import java.lang.reflect.Field;
import java.util.Collection;

/**
 * Created by hgupta on 9/8/16.
 */
public class ZParams {
    public static long MIN_PICKUP_BUFFER_MINS = 120;
    public static long MIN_DELIVERY_BUFFER_MINS = 240;
    public static long MIN_TAT_BUFFER_FOR_VALID_CPT_MINS = MIN_PICKUP_BUFFER_MINS + MIN_DELIVERY_BUFFER_MINS;

    //for linehaul network design
    public static long MAX_ROUTE_TAT_MINS = 60 * 60;//60hrs
    public static int MAX_TOUCH_POINTS = 3;
    public static int MAX_LINEHAULS = 23;

    public static int MAX_TRANSHIPMENTS = 3;
    public static int VEHICLE_STOP_TIME_AT_PC_MINS = 0;
    public static int TRANSHIPMENT_BUFFER_MINS = 120;



    public static void initialize(Collection<ZParam> paramList) throws Exception {
        for(ZParam p: paramList) {
            if(p.getKey() == null) {
                continue;
            }

            Field f = ZParams.class.getDeclaredField(p.getKey());
            if(f == null) {
                throw new Exception("No configuration parameter: " + p.getKey() + " Check sheet: " + ZSheetName.INPUT_ZPARAMS);
            }
            Class<?> t = f.getType();
            if(t == int.class){
                f.setInt(null, Integer.parseInt(p.getValue()));
            }else if(t == long.class){
                f.setLong(null, Long.parseLong(p.getValue()));
            }
        }
    }
}
