package com.rivigo.zoom.lhscheduleoptimiser;

import com.rivigo.zoom.ZParams;
import com.rivigo.zoom.input.dto.ClientPromisedTat;
import com.rivigo.zoom.model.output.RouteSelectionAlgoOutput;
import com.rivigo.zoom.model.common.route.ZRoute;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.util.*;

/**
 * Created by hgupta on 8/8/16.
 */

@Slf4j
@Getter
public class LHScheduleOptimisationAlgo {

    List<ZRoute> linehaulSchedule;
    Set<ZRoute> allRoutes;

    Set<ClientPromisedTat> allCpt;
    Set<ClientPromisedTat> selectedCpt;

    Map<String, Integer> startLocationCount;
    Map<String, Integer> endLocationCount;

    List<RouteSelectionAlgoOutput> output;

    public LHScheduleOptimisationAlgo(Collection<ZRoute> routes,
                                      Collection<ClientPromisedTat> cpts) {
        allRoutes = new HashSet<ZRoute>(routes);
        allCpt = new HashSet<ClientPromisedTat>(cpts);
        startLocationCount = new HashMap<String, Integer>();
        endLocationCount = new HashMap<String, Integer>();

        for(ZRoute r : routes) {
            startLocationCount.put(r.getStartPoint().getLocCode(), 0);
            endLocationCount.put(r.getEndPoint().getLocCode(), 0);
        }
    }

    public void run() {
        //reset
        output = new ArrayList<RouteSelectionAlgoOutput>();
        linehaulSchedule = new ArrayList<ZRoute>();
        selectedCpt = new HashSet<ClientPromisedTat>();

        RouteSelectionAlgo rSelectionAlgo = new RouteSelectionAlgo(allCpt, allRoutes);
        RouteTimeOptimiserAlgo routeTimeOptimiserAlgo = new RouteTimeOptimiserAlgo();

        int seq = 0;
        List<String> startLocOptions = new ArrayList<String>();
        List<String> endLocOptions = new ArrayList<String>();
        while(linehaulSchedule.size() < ZParams.MAX_LINEHAULS
                && selectedCpt.size() != allCpt.size()) {
            seq++;
            startLocOptions.clear(); endLocOptions.clear();
            for(String s : startLocationCount.keySet()) {
                if(startLocationCount.get(s) >= endLocationCount.get(s)) {
                    endLocOptions.add(s);
                }
                if(endLocationCount.get(s) >= startLocationCount.get(s)) {
                    startLocOptions.add(s);
                }
            }
            ZRoute nextBestRoute = rSelectionAlgo.getBestRoute(startLocOptions, endLocOptions);
            List<ClientPromisedTat> possibleCpts = rSelectionAlgo.getCptAssociatedWithRoute(nextBestRoute);
            if(possibleCpts.size() == 0) {
                log.debug("No new cpts for best route " + nextBestRoute);
                break;
            }

            Map<Long, List<ClientPromisedTat>> map =
                    routeTimeOptimiserAlgo.createStartTimeToCptMap(nextBestRoute, possibleCpts);
            Long selectedStartTime = routeTimeOptimiserAlgo.pickStartTime(map);
            List<ClientPromisedTat> cptSatisfied = map.get(selectedStartTime);

            linehaulSchedule.add(nextBestRoute);
            selectedCpt.addAll(cptSatisfied);
            int i = startLocationCount.get(nextBestRoute.getStartPoint().getLocCode());
            startLocationCount.put(nextBestRoute.getStartPoint().getLocCode(), ++i);
            i = endLocationCount.get(nextBestRoute.getEndPoint().getLocCode());
            endLocationCount.put(nextBestRoute.getEndPoint().getLocCode(), ++i);
            rSelectionAlgo.removeCpts(cptSatisfied);

            output.add(new RouteSelectionAlgoOutput(seq, nextBestRoute, selectedStartTime,
                                cptSatisfied.size(),selectedCpt.size(), allCpt.size()));

        }
    }
}
