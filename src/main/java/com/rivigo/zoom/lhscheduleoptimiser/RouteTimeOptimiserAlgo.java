package com.rivigo.zoom.lhscheduleoptimiser;

import com.rivigo.zoom.ZParams;
import com.rivigo.zoom.input.dto.ClientPromisedTat;
import com.rivigo.zoom.model.common.route.ZRoute;
import com.rivigo.zoom.utils.TimeUtils;
import lombok.Getter;

import java.util.*;

/**
 * Created by hgupta on 8/8/16.
 */

@Getter
public class RouteTimeOptimiserAlgo {

    public Map<Long, List<ClientPromisedTat>> createStartTimeToCptMap(ZRoute route, Collection<ClientPromisedTat> cpts) {
        List<TimeOfDayRange> timeRanges = new ArrayList<TimeOfDayRange>();
        for(ClientPromisedTat cpt: cpts) {
            timeRanges.add(getValidLinehaulStartTimeRange(route, cpt));
        }
        Map<Long, List<ClientPromisedTat>> startTimeToCptMap = createStartTimeToCptMap(timeRanges);
        return startTimeToCptMap;
    }

    public Long pickStartTime(Map<Long, List<ClientPromisedTat>> startTimeToCptMap) {
        int max = -1;
        Long selectedStartTime = null;
        for(Long l : startTimeToCptMap.keySet()) {
            List<ClientPromisedTat> cptList = startTimeToCptMap.get(l);
            if(cptList.size() > max) {
                selectedStartTime = l;
            }
        }

        return selectedStartTime;
    }

    private Map<Long,List<ClientPromisedTat>> createStartTimeToCptMap(List<TimeOfDayRange> timeRanges) {
        Set<Long> impIntances = new HashSet<Long>();
        for(TimeOfDayRange t1 : timeRanges) {
            impIntances.add(t1.min);
            impIntances.add(t1.max);
        }

        Map<Long,List<ClientPromisedTat>> retMap = new HashMap<Long, List<ClientPromisedTat>>();
        for(long time : impIntances) {
            List<ClientPromisedTat> cptList = new ArrayList<ClientPromisedTat>();
            for(TimeOfDayRange todr : timeRanges) {
                if(todr.contains(time)) {
                    cptList.add(todr.cpt);
                }
            }
            retMap.put(time, cptList);
        }

        return retMap;
    }

    private TimeOfDayRange getValidLinehaulStartTimeRange(ZRoute route, ClientPromisedTat cpt) {
        TimeOfDayRange range = new TimeOfDayRange();
        range.cpt = cpt;

        long baseTime = 10 * TimeUtils.MILLIS_IN_DAY; //taking day 10 as baseline to keep following timestamps positive
        long pTime = baseTime + cpt.getPickupTime().getMillisOfDay() + ZParams.MIN_PICKUP_BUFFER_MINS * TimeUtils.MILLIS_IN_MINUTE;
        long dTime = baseTime + cpt.getPickupTime().getMillisOfDay() + cpt.getTotalTransitTimeInMillis() - ZParams.MIN_DELIVERY_BUFFER_MINS * TimeUtils.MILLIS_IN_MINUTE;
        long startToDeliveryRouteTat = route.getTatToLocation(cpt.getDestPC());
        long startToPickupRouteTat = route.getTatToLocation(cpt.getSrcPC());
        long minTripStartTime = pTime - startToPickupRouteTat;
        long maxTripStartTime = dTime - startToDeliveryRouteTat;
        if(maxTripStartTime-minTripStartTime > TimeUtils.MILLIS_IN_DAY) {
            range.min = 0;
            range.max = TimeUtils.MILLIS_IN_DAY;
        } else {
            range.min = minTripStartTime % TimeUtils.MILLIS_IN_DAY;
            range.max = maxTripStartTime % TimeUtils.MILLIS_IN_DAY;
        }

        return range;
    }

    class TimeOfDayRange implements Comparable<TimeOfDayRange>{
        private ClientPromisedTat cpt;
        private long min;
        private long max;

        public int compareTo(TimeOfDayRange cmp) {
            if(this.min != cmp.min)
                return (int) (this.min = cmp.min);

            return (int) (this.max - cmp.max);
        }

        public boolean contains(long time) {
            if(min <= max) {
                return min <= time && time <= max;
            } else {
                return (0 <= time && time <= max)
                        || (min <= time && time <= TimeUtils.MILLIS_IN_DAY);
            }
        }
    }
}
