package com.rivigo.zoom.lhscheduleoptimiser.graph;

import com.rivigo.zoom.model.ZLoc;
import com.rivigo.zoom.utils.TimeUtils;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.jgrapht.graph.DefaultWeightedEdge;

/**
 * Created by hgupta on 8/8/16.
 */

public class ZEdge extends DefaultWeightedEdge {
    public ZLoc src;
    public ZLoc dest;
    private Integer weight;

    public ZEdge(ZLoc src, ZLoc dest) {
        super();
        this.src = src;
        this.dest = dest;
        this.weight = null;
    }

    public ZEdge(ZLoc src, ZLoc dest, int weight) {
        this.src = src;
        this.dest = dest;
        this.weight = weight;
    }

    @Override
    public double getWeight() {
        return this.weight;
    }

    @Override
    protected ZLoc getSource() {
        return this.src;
    }

    @Override
    protected ZLoc getTarget() {
        return this.dest;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        ZEdge zEdge = (ZEdge) o;

        return new EqualsBuilder()
                .append(weight, zEdge.weight)
                .append(src, zEdge.src)
                .append(dest, zEdge.dest)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(src)
                .append(dest)
                .append(weight)
                .toHashCode();
    }

    @Override
    public String toString() {
        return src + " -> " + dest + ": " + weight ;
    }

    public long getTimeMillis() {
        return weight * TimeUtils.MILLIS_IN_HOUR;
    }
}
