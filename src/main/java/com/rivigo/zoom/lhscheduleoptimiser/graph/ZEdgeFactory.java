package com.rivigo.zoom.lhscheduleoptimiser.graph;

import com.rivigo.zoom.model.ZLoc;
import org.jgrapht.EdgeFactory;

/**
 * Created by hgupta on 8/8/16.
 */
public class ZEdgeFactory implements EdgeFactory<ZLoc, ZEdge> {

    public ZEdge createEdge(ZLoc src, ZLoc dest) {
        return new ZEdge(src, dest);
    }
}
