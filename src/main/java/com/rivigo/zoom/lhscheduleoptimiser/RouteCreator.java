package com.rivigo.zoom.lhscheduleoptimiser;

import com.rivigo.zoom.ZParams;
import com.rivigo.zoom.model.ZRouteStop;
import com.rivigo.zoom.model.common.route.ZRoute;
import com.rivigo.zoom.input.dto.LocationInput;
import com.rivigo.zoom.input.dto.InputHop;
import com.rivigo.zoom.utils.TimeUtils;

import java.util.*;

/**
 * Created by hgupta on 8/8/16.
 */
public class RouteCreator {

    public static List<ZRoute> createPossibleRoutes(Collection<LocationInput> locationInputs, Collection<InputHop> possibleHops) {
        Map<String, List<InputHop>> outgoingEdges = new HashMap<String, List<InputHop>>();
        for(InputHop h : possibleHops) {
            List<InputHop> hopList = outgoingEdges.get(h.getStartLoc());
            if(hopList == null) {
                hopList = new ArrayList<InputHop>();
                outgoingEdges.put(h.getStartLoc(), hopList);
            }
            hopList.add(h);
        }

        Map<Integer, List<ZRoute>> routes = new HashMap<Integer, List<ZRoute>>();
        populateRouteMap(outgoingEdges, routes);
        List<ZRoute> validRoutes = getValidRoutes(routes, locationInputs);

        return validRoutes;
    }

    private static List<ZRoute> getValidRoutes(Map<Integer, List<ZRoute>> routeMap, Collection<LocationInput> locationInputs) {
        List<String> endpoints = new ArrayList<String>();
        for(LocationInput pc : locationInputs) {
            if(pc.getIsEndPoint() == 1)
                endpoints.add(pc.getLocationCode());
        }
        List<ZRoute> retMap = new ArrayList<ZRoute>();
        for(List<ZRoute> rList : routeMap.values()) {
            for(ZRoute r : rList) {
                if(isRouteValid(r, endpoints, endpoints)) {
                    retMap.add(r);
                }
            }
        }

        return retMap;
    }

    private static boolean isRouteValid(ZRoute r, List<String> startPcList, List<String> endPcList) {
        ZRouteStop start = r.getStartPoint();
        ZRouteStop end = r.getEndPoint();

        if(!startPcList.contains(start.getLocCode()))
            return false;

        if(!endPcList.contains(end.getLocCode()))
            return false;

        if(r.getTotalTat() > ZParams.MAX_ROUTE_TAT_MINS * TimeUtils.MILLIS_IN_MINUTE) {
            return false;
        }

        if(r.getTouchPointCount() > ZParams.MAX_TOUCH_POINTS)
            return false;

        Set<String> stops = new HashSet<String>();
        for(ZRouteStop s : r.getStops()) {
            if(stops.contains(s.getLocCode())) {
                return false;
            }
            stops.add(s.getLocCode());
        }

        return true;
    }

    public static void populateRouteMap(Map<String, List<InputHop>> outgoingEdges, Map<Integer, List<ZRoute>> routes) {
        for(int tp = 0; tp <= ZParams.MAX_TOUCH_POINTS; tp++) {
            List<ZRoute> tpHopRoutes = new ArrayList<ZRoute>();
            if(tp == 0) {
                for(List<InputHop> hopList : outgoingEdges.values()) {
                    for(InputHop h : hopList) {
                        ZRoute r = new ZRoute(h);
                        tpHopRoutes.add(r);
                    }
                }
            } else {
                List<ZRoute> oneLessHopRoutes = routes.get(tp - 1);
                for(ZRoute r : oneLessHopRoutes) {
                    ZRouteStop endPoint = r.getEndPoint();
                    List<InputHop> outgoingHops = outgoingEdges.get(endPoint.getLocCode());
                    if(outgoingHops == null) {
                        System.out.println(endPoint.getLocCode());
                    }
                    for(InputHop h  : outgoingHops) {
                        //TODO: himanshu: fix this shit
//                        ZRoute rClone = r.clone();
//                        ZRouteStop newStop = new ZRouteStop(r.getRouteCode(), h.getEndLoc(), -1);
//                        rClone.addToEnd(newStop);
//                        tpHopRoutes.add(rClone);
                    }
                }
            }
            routes.put(tp, tpHopRoutes);
        }
    }

}
