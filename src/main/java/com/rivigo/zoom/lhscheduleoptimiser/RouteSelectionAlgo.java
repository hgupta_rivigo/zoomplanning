package com.rivigo.zoom.lhscheduleoptimiser;

import com.rivigo.zoom.ZParams;
import com.rivigo.zoom.input.dto.ClientPromisedTat;
import com.rivigo.zoom.model.output.ClientPromisedTatFormat1;
import com.rivigo.zoom.model.common.route.ZRoute;
import com.rivigo.zoom.utils.TimeUtils;

import java.util.*;

/**
 * Created by hgupta on 8/8/16.
 */
public class RouteSelectionAlgo {
    Map<ClientPromisedTat, List<ZRoute>> cptToRouteMap;
    Map<ZRoute, List<ClientPromisedTat>> routeToCptMap;
    public RouteSelectionAlgo(Collection<ClientPromisedTat> cpts, Collection<ZRoute> routes) {
        cptToRouteMap = new HashMap<ClientPromisedTat, List<ZRoute>>();
        routeToCptMap = new HashMap<ZRoute, List<ClientPromisedTat>>();

        populateMaps(cpts, routes);
    }

    private void populateMaps(Collection<ClientPromisedTat> cpts,
                                       Collection<ZRoute> routes) {
        if(cpts.isEmpty()) {
            throw  new RuntimeException("No Client Contracts Found");
        }
        if(routes.isEmpty()) {
            throw  new RuntimeException("No routes Found");
        }
        Map<String, List<ZRoute>> connectionRouteMap = ZRoute.getMap1(routes);
        for(ClientPromisedTat cpt: cpts) {
            List<ZRoute> rList = connectionRouteMap.get(cpt.getSrcPC() + "-" + cpt.getDestPC());
            long cptTat = cpt.getTotalTransitTimeInMillis();
            if(rList == null) {
                continue;
            }
            for(ZRoute r : rList) {
                long routeTat = r.getTatBetweenLocations(cpt.getSrcPC(), cpt.getDestPC());
                if(cptTat - routeTat > ZParams.MIN_TAT_BUFFER_FOR_VALID_CPT_MINS * TimeUtils.MILLIS_IN_MINUTE) {
                    addToMaps(cpt, r);
                }
            }
        }
    }

    private void addToMaps(ClientPromisedTat cpt, ZRoute r) {
        List<ZRoute> rList = cptToRouteMap.get(cpt);
        if(rList == null) {
            rList = new ArrayList<ZRoute>();
            cptToRouteMap.put(cpt, rList);
        }
        rList.add(r);

        List<ClientPromisedTat> cptList = routeToCptMap.get(r);
        if(cptList == null) {
            cptList = new ArrayList<ClientPromisedTat>();
            routeToCptMap.put(r, cptList);
        }
        cptList.add(cpt);
    }

    public List<ClientPromisedTatFormat1> getCptFormat1() {
        List<ClientPromisedTatFormat1> retList = new ArrayList<ClientPromisedTatFormat1>();

        for(ClientPromisedTat cpt : cptToRouteMap.keySet()) {
            List<ZRoute> rList = cptToRouteMap.get(cpt);
            ClientPromisedTatFormat1 cptF1 = new ClientPromisedTatFormat1(cpt, rList.size());
            retList.add(cptF1);
        }

        return retList;
    }

    public ZRoute getBestRoute(List<String> startLocations, List<String> endLocations) {
        List<ZRoute> allRoutes = new ArrayList<ZRoute>(routeToCptMap.keySet());
        Iterator<ZRoute> it = allRoutes.iterator();
        while(it.hasNext()) {
            ZRoute r = it.next();
            if(startLocations.contains(r.getStartPoint().getLocCode()) == false
                    || endLocations.contains(r.getEndPoint().getLocCode()) == false) {
                it.remove();
            }
        }
        if(allRoutes.isEmpty()) {
            System.out.println("Something is wrong!!");
        }
        Collections.sort(allRoutes, new Comparator<ZRoute>() {
            public int compare(ZRoute zRoute1, ZRoute zRoute2) {
                List<ClientPromisedTat> cptList1 = routeToCptMap.get(zRoute1);
                List<ClientPromisedTat> cptList2 = routeToCptMap.get(zRoute2);
                int count1 = cptList1 == null? 0 : cptList1.size();
                int count2 = cptList2 == null? 0 : cptList2.size();
                return count1 - count2;
            }
        });

        return allRoutes.get(allRoutes.size()-1);
    }


    public List<ClientPromisedTat> getCptAssociatedWithRoute(ZRoute route) {
        return routeToCptMap.get(route);
    }

    public void removeRoute(ZRoute route) {
        List<ClientPromisedTat> cptList = routeToCptMap.remove(route);
        for(ClientPromisedTat cpt : cptList) {
            cptToRouteMap.get(cpt).remove(route);
        }

    }
    public void removeCpt(ClientPromisedTat cpt) {
        List<ZRoute> routeList = cptToRouteMap.remove(cpt);
        for(ZRoute route : routeList) {
            routeToCptMap.get(route).remove(cpt);
        }

    }

    public void removeCpts(List<ClientPromisedTat> cpts) {
        for(ClientPromisedTat cpt : cpts) {
            removeCpt(cpt);
        }
    }

    public int getAssociatedRouteCount(ClientPromisedTat cpt) {
        if(!cptToRouteMap.containsKey(cpt)) {
            return -1;
        }
        if(cptToRouteMap.get(cpt) == null)
            return 0;

        return cptToRouteMap.get(cpt).size();
    }
}
