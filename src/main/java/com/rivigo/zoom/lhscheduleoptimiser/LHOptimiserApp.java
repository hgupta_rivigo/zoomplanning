package com.rivigo.zoom.lhscheduleoptimiser;

import com.rivigo.common.report.impl.GenericReportGeneratorImpl;
import com.rivigo.zoom.ZParams;
import com.rivigo.zoom.datatransformation.algo.PickupDeliveryAnalyser;
import com.rivigo.zoom.input.dto.TripScheduleInput;
import com.rivigo.zoom.input.dto.ClientPromisedTat;
import com.rivigo.zoom.input.dto.InputHop;
import com.rivigo.zoom.input.dto.LocationInput;
import com.rivigo.zoom.input.dto.ZParam;
import com.rivigo.zoom.model.output.ClientPromisedTatFormat1;
import com.rivigo.zoom.model.output.RouteSelectionAlgoOutput;
import com.rivigo.zoom.model.common.route.ZRoute;
import com.rivigo.zoom.model.output.ZRouteFormat1;
import com.rivigo.zoom.LinehaulNetwork;
import com.rivigo.zoom.utils.ExcelUtils;
import com.rivigo.zoom.utils.TimeUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

import java.io.File;
import java.util.*;

/**
 * Created by hgupta on 2/8/16.
 */
@Slf4j
public class LHOptimiserApp {
    public static String INPUT_FILE = "src/main/resources/linehaul_route_optimiser.xlsx";
    public static String OUTPUT_FILE = "target/output";

    public static LinehaulNetwork linehaulNetwork;

    public static void main(String[] args) throws Exception  {
        File f = new File(INPUT_FILE);
        Workbook inputWB = WorkbookFactory.create(f);
        SXSSFWorkbook outputWB = new SXSSFWorkbook();

        //================================= INPUT =================================================//
        List<ZParam> params = ExcelUtils.readFromSheet(inputWB, "params", ZParam.class);
        ZParams.initialize(params);

        List<ClientPromisedTat> allCpt = ExcelUtils.readFromSheet(inputWB, "client_contract", ClientPromisedTat.class);
        log.info("Total CPT read: " + allCpt.size());
        List<LocationInput> pcList = ExcelUtils.readFromSheet(inputWB, "locations", LocationInput.class);
        List<InputHop> tats = ExcelUtils.readFromSheet(inputWB, "tats", InputHop.class);
        //================================= INPUT VALIDATION =================================================//

        if(pcList == null || pcList.isEmpty()) throw new RuntimeException("locations tab is empty");
        if(tats == null || tats.isEmpty()) throw new RuntimeException("tats tab is empty");
        linehaulNetwork = new LinehaulNetwork(tats);

        //================================================================================================//


        Map<ClientPromisedTat, ClientPromisedTatFormat1> o1 = new HashMap<ClientPromisedTat, ClientPromisedTatFormat1>();
        List<ClientPromisedTat> filteredCptList = new ArrayList<ClientPromisedTat>();

        for(ClientPromisedTat cpt: allCpt) {
            try {
                double minTat = linehaulNetwork.getMinTatHrs(cpt.getSrcPC(), cpt.getDestPC());
                ClientPromisedTatFormat1 cptF1 = new ClientPromisedTatFormat1(cpt, minTat, true);
                o1.put(cpt, cptF1);
                if(cpt.getSrcPC().equals(cpt.getDestPC())) {
                    continue;
                } else if(Double.isInfinite(minTat)) {
                    cptF1.setValidBuffer(false);
                    continue;
                } else if(cpt.getTotalTransitTimeInMillis() - minTat * TimeUtils.MILLIS_IN_HOUR < ZParams.MIN_TAT_BUFFER_FOR_VALID_CPT_MINS * TimeUtils.MILLIS_IN_MINUTE) {
                    cptF1.setValidBuffer(false);
                    continue;
                }
                filteredCptList.add(cpt);
            } catch (Exception ex) {
                log.error("Error due to " + cpt, ex);
                throw ex;
            }
        }
        log.info("CPT count without src same as dest, unreachable rivigo network, impossible tats: " + filteredCptList.size());

        List<ZRoute> possibleRoutes = RouteCreator.createPossibleRoutes(pcList, tats);
        log.info("Total Possible Routes: " + possibleRoutes.size());
        publishPossibleRoutes(possibleRoutes, outputWB);
        publishRouteToCptMap(o1, possibleRoutes, outputWB);

        LHScheduleOptimisationAlgo algo = new LHScheduleOptimisationAlgo(possibleRoutes, filteredCptList);
        algo.run();
       List<ZRoute> selectedRoutes = algo.getLinehaulSchedule();

        List<RouteSelectionAlgoOutput> outAlgo = algo.getOutput();
        GenericReportGeneratorImpl.write(outputWB.createSheet("Trip Schedule"), RouteSelectionAlgoOutput.class, outAlgo);

        runPickupDeliveryAnalyser(allCpt, outAlgo, outputWB);

        ExcelUtils.writeToFile(outputWB, OUTPUT_FILE);
    }

    private static void runPickupDeliveryAnalyser(List<ClientPromisedTat> cptList,
                                                  List<RouteSelectionAlgoOutput> outAlgo,
                                                  SXSSFWorkbook outputWB) {
        List<TripScheduleInput> tripSchedule = new ArrayList<TripScheduleInput>();
        for(int i=0; i<outAlgo.size(); i++) {
            TripScheduleInput ts = new TripScheduleInput();
            ts.clientRoute = outAlgo.get(i).getRouteSelected();
            ts.dispatchTime = outAlgo.get(i).getStartTime();

            tripSchedule.add(ts);
        }
        PickupDeliveryAnalyser.run(cptList, tripSchedule, linehaulNetwork, outputWB);
    }

    private static void publishPossibleRoutes(List<ZRoute> possibleRoutes, SXSSFWorkbook outputWB) {
        List<ZRouteFormat1> outRoutes = ZRoute.getOutputFormat1(possibleRoutes);
        GenericReportGeneratorImpl.write(outputWB.createSheet("out_routes"), ZRouteFormat1.class, outRoutes);
    }

    private static void publishRouteToCptMap(Map<ClientPromisedTat, ClientPromisedTatFormat1> cptMap, List<ZRoute> possibleRoutes,
                                             SXSSFWorkbook outputWB) {
        RouteSelectionAlgo routeSelectionAlgo = new RouteSelectionAlgo(cptMap.keySet(), possibleRoutes);
        for(ClientPromisedTat cpt: cptMap.keySet()) {
            int routeCount = routeSelectionAlgo.getAssociatedRouteCount(cpt);
            cptMap.get(cpt).setNoOfRoutes(routeCount);
        }
        GenericReportGeneratorImpl.write(outputWB.createSheet("out_cpt2"), ClientPromisedTatFormat1.class, cptMap.values());
    }
}
