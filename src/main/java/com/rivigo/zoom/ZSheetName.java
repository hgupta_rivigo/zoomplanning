package com.rivigo.zoom;

/**
 * Created by hgupta on 9/8/16.
 */

public class ZSheetName {
    private ZSheetName(){}

    public static final String INPUT_WORKBOOK = "zoom_linehaul_analysis.xlsx";

    public static final String INPUT_CLIENT_PROMISED_TAT = "client_contracts_i";
    public static final String INPUT_ZPARAMS = "params_i";
    public static final String INPUT_TRIP_SCHEDULES = "trip_schedules_i";
    public static final String INPUT_TATS = "tats_i";
    public static final String OUTPUT_CLIENT_PROMISED_TAT = "client_contracts_out";
    public static final String OUTPUT_TRIP_SCHEDULES = "trip_schedules_out";

}
