package com.rivigo.zoom;

import com.rivigo.zoom.lhscheduleoptimiser.graph.ZEdge;
import com.rivigo.zoom.lhscheduleoptimiser.graph.ZEdgeFactory;
import com.rivigo.zoom.model.ZLoc;
import com.rivigo.zoom.input.dto.InputHop;
import com.rivigo.zoom.input.dto.LocationInput;
import com.rivigo.zoom.model.ZRouteStop;
import com.rivigo.zoom.utils.TimeUtils;
import lombok.extern.slf4j.Slf4j;
import org.jgrapht.alg.DijkstraShortestPath;
import org.jgrapht.graph.SimpleDirectedWeightedGraph;

import java.util.*;

/**
 * Created by hgupta on 9/8/16.
 */

@Slf4j
public class LinehaulNetwork {
    SimpleDirectedWeightedGraph<ZLoc, ZEdge> graph = null;

   public LinehaulNetwork(List<InputHop> tats) {
        Set<String> locations = new HashSet<String>();
        for(InputHop hop: tats) {
            locations.add(hop.getStartLoc());
            locations.add(hop.getEndLoc());
        }

        List<LocationInput> pcList = new ArrayList<LocationInput>();
        for(String s: locations)  {
            pcList.add(new LocationInput(s, 1, 1));
        }

        buildRouteNetworkGraph(pcList, tats);
        validateNetwork(tats);
    }

    public double getMinTatHrs(String start, String end) {
        DijkstraShortestPath dsp = new DijkstraShortestPath(graph, new ZLoc(start),
                                                                new ZLoc(end));
        double pathLenth = dsp.getPathLength();
        if(Double.isInfinite(pathLenth)){
            return pathLenth;
        }
        List<ZEdge> edgeList  = dsp.getPathEdgeList();
        int totalHoldingTimeHours = (int) ((edgeList.size() - 1) * (ZParams.VEHICLE_STOP_TIME_AT_PC_MINS /60));
        return (dsp.getPathLength() + totalHoldingTimeHours) ;
    }
    public Long getDirectTatMillis(String start, String end) {
        ZLoc startLoc = ZCache.getZLoc(start);
        ZLoc endLoc = ZCache.getZLoc(end);
        ZEdge edge = graph.getEdge(startLoc, endLoc);
        if(edge == null) {
            log.error("Direct not found for " + start + " -> " + end);
            return null;
        }
        return edge.getTimeMillis();
    }
    public long getDirectTatMillis(ZRouteStop start, ZRouteStop end) {
        ZEdge edge = graph.getEdge(start.getZLoc(), end.getZLoc());
        return edge.getTimeMillis();
    }

    private void validateNetwork(List<InputHop> hops) {
        for(InputHop h: hops) {
            DijkstraShortestPath dsp = new DijkstraShortestPath(graph, new ZLoc(h.getStartLoc()),
                                                                        new ZLoc(h.getEndLoc()));
            double minTat = dsp.getPathLength();
            if(h.getTatHrs() > minTat) {
                log.warn("BAD TAT {} : Indirect path is takes lesser time {} ",h ,dsp.getPathEdgeList());
            }
        }
    }

    private SimpleDirectedWeightedGraph<ZLoc, ZEdge> buildRouteNetworkGraph(Collection<LocationInput> pcList,
                                                                            List<InputHop> tats) {
        graph = new SimpleDirectedWeightedGraph(ZEdgeFactory.class);
        for(LocationInput pc : pcList) {
            graph.addVertex(new ZLoc(pc.getLocationCode()));
        }
        for(InputHop arc : tats) {
            ZLoc src = new ZLoc(arc.startLoc);
            ZLoc dest = new ZLoc(arc.endLoc);
            ZEdge edge = new ZEdge(src, dest, arc.tatHrs);
            try {
                boolean r = graph.addEdge(src, dest, edge);
                if (r == false) {
                    System.out.print("Edge Already present" + edge);
                }
            } catch (Exception ex) {
            }

            graph.setEdgeWeight(edge, edge.getWeight());
        }
        return graph;
    }
}
