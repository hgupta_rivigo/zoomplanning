package com.rivigo.zoom.input.reader;

import com.rivigo.common.report.impl.GenericReportReaderImpl;
import com.rivigo.zoom.input.dto.ClientPromisedTat;
import com.rivigo.zoom.input.dto.TripScheduleInput;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.Sheet;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by hgupta on 18/8/16.
 */
@Slf4j
public class ClientPromisedTatReader {
    private ClientPromisedTatReader() {}

    public static List<ClientPromisedTat> readExcel(Sheet sheet) {
        List<ClientPromisedTat> retList = new ArrayList<ClientPromisedTat>(GenericReportReaderImpl.read(sheet, ClientPromisedTat.class));
        retList = removeDuplicates(retList);
        return retList;
    }

    private static List<ClientPromisedTat> removeDuplicates(List<ClientPromisedTat> cpts) {
        int duplicateCount = 0;
        List<ClientPromisedTat> retList = new ArrayList<ClientPromisedTat>();
        for(ClientPromisedTat cpt  : cpts) {
            if(!retList.contains(cpt)) {
                retList.add(cpt);
            } else {
                duplicateCount++;
            }
        }

        log.info("Removing {} duplicate CPT", duplicateCount);
        return retList;
    }

    public static List<ClientPromisedTat> removeSameSrcDest(List<ClientPromisedTat> cpts) {
        int sameSrcDest = 0;
        List<ClientPromisedTat> retList = new ArrayList<ClientPromisedTat>();
        Iterator<ClientPromisedTat> it = cpts.iterator();
        while(it.hasNext()) {
            ClientPromisedTat n = it.next();
            if(n.getSrcPC().equals(n.getDestPC()) == false) {
                retList.add(n);
            } else {
                sameSrcDest++;
            }
        }

        log.info("Removing {} same src desc CPT", sameSrcDest);
        return  retList;
    }


}
