package com.rivigo.zoom.input.reader;

import com.rivigo.common.report.impl.GenericReportReaderImpl;
import com.rivigo.zoom.input.dto.TatInput;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.Sheet;

import java.util.*;

/**
 * Created by hgupta on 18/8/16.
 */
@Slf4j
public class TatReader {
    private TatReader() {}

    public static List<TatInput> readExcel(Sheet sheet) {
        List<TatInput> retList = new ArrayList<TatInput>(GenericReportReaderImpl.read(sheet, TatInput.class));
        validateTatInput(retList);
        return retList;
    }

    public static boolean validateTatInput(Collection<TatInput> tats) {
        Map<String, TatInput> map = new HashMap<String, TatInput>();
        boolean ret = true;
        for(TatInput tat : tats) {
            TatInput tatDup = map.get(tat.getStartLoc() + "-" + tat.getEndLoc());
            if(tatDup != null) {
                log.error("Duplicate tat: " + tat + tatDup);
                ret = false;
            }

            TatInput tatRev = map.get(tat.getEndLoc() + "-" + tat.getStartLoc());
            if(tatRev != null) {
                if(tat.getTat_22ft() != tatRev.getTat_22ft()
                   || tat.getTat_32ft() != tatRev.getTat_32ft()) {
                    log.error("Tat back and forth are not same");
                }
            }

        }

        return ret;

    }
}
