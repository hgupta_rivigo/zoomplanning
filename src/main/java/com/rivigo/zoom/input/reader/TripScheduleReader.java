package com.rivigo.zoom.input.reader;

import com.rivigo.common.report.impl.GenericReportReaderImpl;
import com.rivigo.zoom.input.dto.TatInput;
import com.rivigo.zoom.input.dto.TripScheduleInput;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.Sheet;

import java.util.*;

/**
 * Created by hgupta on 18/8/16.
 */
@Slf4j
public class TripScheduleReader {
    private TripScheduleReader() {}

    public static List<TripScheduleInput> readExcel(Sheet sheet) {
        List<TripScheduleInput> retList = new ArrayList<TripScheduleInput>(GenericReportReaderImpl.read(sheet, TripScheduleInput.class));
        retList = updateRouteNamesForZoom(retList);
        if(isValid(retList) == false) {
            throw  new RuntimeException();
        }
        return retList;
    }

    private static List<TripScheduleInput> updateRouteNamesForZoom(List<TripScheduleInput> tripSchedules) {
        for(TripScheduleInput ts : tripSchedules) {
            ts.clientRoute = ts.clientRoute.replaceAll("RZM_", "");
            ts.clientRoute = ts.clientRoute.replaceAll("DELT2", "DELT1");
            ts.clientRoute = ts.clientRoute.replaceAll("DELT1-DELT1", "DELT1");
        }
        return tripSchedules;
    }

    private static boolean isValid(Collection<TripScheduleInput> inputs) {
        List<TripScheduleInput> set = new ArrayList<TripScheduleInput>();
        for(TripScheduleInput i : inputs) {
            if(set.contains(i)) {
                return false;
            }

            set.add(i);
        }

        return true;
    }
}
