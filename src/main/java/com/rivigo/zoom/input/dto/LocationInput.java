package com.rivigo.zoom.input.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by hgupta on 29/7/16.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class LocationInput {
    private String locationCode;
    private int holdingTimeHrs;
    private int isEndPoint;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        LocationInput locationInput = (LocationInput) o;

        return new EqualsBuilder()
                .append(locationCode, locationInput.locationCode)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(locationCode)
                .toHashCode();
    }

    public static Map<String, LocationInput> createMap(Collection<LocationInput> pcs) {
        Map<String, LocationInput> retMap = new HashMap<String, LocationInput>();
        for(LocationInput pc : pcs) {
            retMap.put(pc.getLocationCode(), pc);
        }

        return retMap;
    }

    @Override
    public String toString() {
        return locationCode;
    }
}
