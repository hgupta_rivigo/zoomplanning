package com.rivigo.zoom.input.dto;

import com.rivigo.zoom.annotations.NoSpaceString;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.joda.time.LocalTime;

/**
 * Created by hgupta on 29/7/16.
 */
@Getter
@Setter
public class ClientPromisedTat {
    public String client;

    @NoSpaceString
    public String srcPC;

    @NoSpaceString
    public String destPC;
    public LocalTime pickupTime;
    public int tatDays;
    public LocalTime deliveryTime;
    public double loadKg;
    public int priority;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        ClientPromisedTat that = (ClientPromisedTat) o;

        return new EqualsBuilder()
                .append(tatDays, that.tatDays)
                .append(client, that.client)
                .append(srcPC, that.srcPC)
                .append(destPC, that.destPC)
                .append(pickupTime, that.pickupTime)
                .append(deliveryTime, that.deliveryTime)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(client)
                .append(srcPC)
                .append(destPC)
                .append(pickupTime)
                .append(tatDays)
                .append(deliveryTime)
                .toHashCode();
    }

    public long getPickupTimeMillis() {
        return pickupTime.getMillisOfDay();
    }
    public long getTotalTransitTimeInMillis() {
        return (24 * 3600 * 1000l)- pickupTime.getMillisOfDay()
                + (tatDays - 1) * (24 * 3600 * 1000l)
                + deliveryTime.getMillisOfDay()  ;
    }
    public long getDeliveryTimeAbsolute() {
        return pickupTime.getMillisOfDay() + getTotalTransitTimeInMillis();
    }

    @Override
    public String toString() {
        return client + ": " + srcPC + "=> "+ destPC + " : " + pickupTime;
    }
}
