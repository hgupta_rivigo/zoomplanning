package com.rivigo.zoom.input.dto;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

/**
 * Created by hgupta on 18/8/16.
 */

@Getter
@Setter
public class TatInput {
    private String startLoc;
    private String endLoc;
    private int tat_22ft;
    private int tat_32ft;

    @Override
    public String toString() {
        return startLoc + "-" + endLoc + ": " + tat_22ft + "|" + tat_32ft;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        TatInput tatInput = (TatInput) o;

        return new EqualsBuilder()
                .append(tat_22ft, tatInput.tat_22ft)
                .append(tat_32ft, tatInput.tat_32ft)
                .append(startLoc, tatInput.startLoc)
                .append(endLoc, tatInput.endLoc)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(startLoc)
                .append(endLoc)
                .append(tat_22ft)
                .append(tat_32ft)
                .toHashCode();
    }
}
