package com.rivigo.zoom.input.dto;

import com.rivigo.zoom.annotations.NoSpaceString;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

/**
 * Created by hgupta on 29/7/16.
 */
@Getter
@Setter
public class InputHop {
    @NoSpaceString
    public String startLoc;

    @NoSpaceString
    public String endLoc;

    public int tatHrs;

    @Override
    public String toString() {
        return this.startLoc + "->" + this.endLoc + ": " + tatHrs;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        InputHop hop = (InputHop) o;

        return new EqualsBuilder()
                .append(startLoc, hop.startLoc)
                .append(endLoc, hop.endLoc)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(startLoc)
                .append(endLoc)
                .toHashCode();
    }
}
