package com.rivigo.zoom.input.dto;

import com.rivigo.zoom.annotations.NoSpaceString;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by hgupta on 9/8/16.
 */
@Getter
@Setter
public class ZParam {
    @NoSpaceString
    public String key;

    @NoSpaceString
    public String value;
}
