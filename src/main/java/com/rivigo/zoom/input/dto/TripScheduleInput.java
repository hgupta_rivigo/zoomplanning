package com.rivigo.zoom.input.dto;

import com.rivigo.zoom.annotations.NoSpaceString;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.joda.time.LocalTime;

/**
 * Created by hgupta on 29/7/16.
 */
@Getter
@Setter
public class TripScheduleInput {
    public Integer scheduleId;

    @NoSpaceString
    public String clientRoute;

    public LocalTime dispatchTime;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        TripScheduleInput that = (TripScheduleInput) o;

        return new EqualsBuilder()
                .append(clientRoute, that.clientRoute)
                .append(dispatchTime, that.dispatchTime)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(clientRoute)
                .append(dispatchTime)
                .toHashCode();
    }
}
