package com.rivigo.zoom.ClientContractCalculator;

import com.rivigo.zoom.ZParams;
import com.rivigo.zoom.model.TripLeg;
import com.rivigo.zoom.model.ZTripStop;
import com.rivigo.zoom.utils.TimeUtils;
import lombok.Getter;
import lombok.Setter;
import org.joda.time.LocalTime;

import java.util.List;

/**
 * Created by hgupta on 12/8/16.
 */
@Getter
@Setter
public class CptCalculatorAppOut {
    public String srcLoc;
    public String destLoc;
    public LocalTime pickupCutoff;
    public int tatDays;
    public LocalTime deliveryCutoff;
    public int tatHrs;
    public CptCalculatorAppOut(ZTripStop start, ZTripStop end) {
        this.srcLoc = start.getLocCode();
        this.destLoc = end.getLocCode();
        long pickupCutoff = start.getDepartureTime() - ZParams.MIN_PICKUP_BUFFER_MINS * TimeUtils.MILLIS_IN_MINUTE;
        long deliveryCutoff = end.getArrivalTime() + ZParams.MIN_DELIVERY_BUFFER_MINS * TimeUtils.MILLIS_IN_MINUTE;
        if(pickupCutoff < 0) {
            pickupCutoff += TimeUtils.MILLIS_IN_DAY;
            deliveryCutoff += TimeUtils.MILLIS_IN_DAY;
        }
        this.pickupCutoff = TimeUtils.getLocalTime(pickupCutoff);
        this.deliveryCutoff = TimeUtils.getLocalTime(deliveryCutoff);
        this.tatHrs = (int) ((deliveryCutoff - pickupCutoff)/TimeUtils.MILLIS_IN_HOUR);
        this.tatDays = (int) (deliveryCutoff/TimeUtils.MILLIS_IN_DAY -
                                pickupCutoff/TimeUtils.MILLIS_IN_DAY);

    }

    public CptCalculatorAppOut(List<TripLeg> option) {
        TripLeg startLeg = option.get(0);
        TripLeg endLeg = option.get(option.size() - 1);

        this.srcLoc = startLeg.srcLoc;
        this.destLoc = endLeg.destLoc;
        long pickupCutoff = startLeg.getStartTimeMillis() - ZParams.MIN_PICKUP_BUFFER_MINS * TimeUtils.MILLIS_IN_MINUTE;
        long deliveryCutoff = endLeg.getEndTimeMillis() + ZParams.MIN_DELIVERY_BUFFER_MINS * TimeUtils.MILLIS_IN_MINUTE;
        if(pickupCutoff < 0) {
            pickupCutoff += TimeUtils.MILLIS_IN_DAY;
            deliveryCutoff += TimeUtils.MILLIS_IN_DAY;
        }
        this.pickupCutoff = TimeUtils.getLocalTime(pickupCutoff);
        this.deliveryCutoff = TimeUtils.getLocalTime(deliveryCutoff);
        this.tatHrs = (int) ((deliveryCutoff - pickupCutoff)/TimeUtils.MILLIS_IN_HOUR);
        this.tatDays = (int) (deliveryCutoff/TimeUtils.MILLIS_IN_DAY -
                pickupCutoff/TimeUtils.MILLIS_IN_DAY);

    }
}
