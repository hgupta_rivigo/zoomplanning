package com.rivigo.zoom.ClientContractCalculator;

import com.rivigo.common.report.impl.GenericReportGeneratorImpl;
import com.rivigo.zoom.ZParams;
import com.rivigo.zoom.ZSheetName;
import com.rivigo.zoom.datatransformation.algo.LinehaulDAG;
import com.rivigo.zoom.datatransformation.algo.LinehaulDAG_v2;
import com.rivigo.zoom.datatransformation.algo.PickupDeliveryAnalyser;
import com.rivigo.zoom.model.TripLeg;
import com.rivigo.zoom.input.dto.TripScheduleInput;
import com.rivigo.zoom.input.dto.ClientPromisedTat;
import com.rivigo.zoom.model.common.trip.ZTrip;
import com.rivigo.zoom.model.ZTripStop;
import com.rivigo.zoom.model.common.trip.ZTripStopOut1;
import com.rivigo.zoom.input.dto.InputHop;
import com.rivigo.zoom.input.dto.LocationInput;
import com.rivigo.zoom.input.dto.ZParam;
import com.rivigo.zoom.LinehaulNetwork;
import com.rivigo.zoom.utils.ExcelUtils;
import com.rivigo.zoom.utils.TimeUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

import java.io.File;
import java.util.*;

/**
 * Created by hgupta on 12/8/16.
 */
@Slf4j
public class CptCalculatorApp {
    public static String OUTPUT_FILE = "cpt_calculator_";

    public static void main(String[] args) throws Exception {
        File f = new File(ZSheetName.INPUT_WORKBOOK);
        Workbook inputWB = WorkbookFactory.create(f);
        SXSSFWorkbook outputWB = new SXSSFWorkbook();

        //================================= INPUT =================================================//
        List<ZParam> params = ExcelUtils.readFromSheet(inputWB, ZSheetName.INPUT_ZPARAMS, ZParam.class);
        ZParams.initialize(params);
        List<TripScheduleInput> tripSchedules = ExcelUtils.readFromSheet(inputWB, ZSheetName.INPUT_TRIP_SCHEDULES, TripScheduleInput.class);
        List<InputHop> tats = ExcelUtils.readFromSheet(inputWB, ZSheetName.INPUT_TATS, InputHop.class);

        LinehaulNetwork linehaulNetwork = new LinehaulNetwork(tats);
        List<ZTrip> zTrips = new ArrayList<ZTrip>();
        for (TripScheduleInput ts : tripSchedules) {
            zTrips.add(new ZTrip(ts, linehaulNetwork));
        }
        ZTripStopOut1.publishTrips(zTrips, outputWB.createSheet("Trip Stops"));
        log.info("===========================================================");
        log.info("                             TRIPS PUBLISHED               ");
        log.info("===========================================================");
        List<CptCalculatorAppOut> directOut = generateDirectRoutes(zTrips);
        log.info("===========================================================");
        log.info("                                      Direct TATs Calculated                                  ");
        log.info("===========================================================");
       List<CptCalculatorAppOut> indirectOut = generateIndirectRoutes(zTrips, tripSchedules, linehaulNetwork);
    log.info("===========================================================");
    log.info("                  InDirect TATs Calculated                 ");
    log.info("===========================================================");
    GenericReportGeneratorImpl.write(outputWB.createSheet("Direct Proposals"), CptCalculatorAppOut.class, directOut);
    log.info("===========================================================");
    log.info("                  Direct TATs Published                 ");
    log.info("===========================================================");
    GenericReportGeneratorImpl.write(outputWB.createSheet("Trans Proposals"), CptCalculatorAppOut.class, indirectOut);
    log.info("===========================================================");
    log.info("          InDirect TATs Published         ");
    log.info("===========================================================");
    ExcelUtils.writeToFile(outputWB, OUTPUT_FILE);
    }

    public static List<CptCalculatorAppOut> generateDirectRoutes(List<ZTrip> trips) {
        List<CptCalculatorAppOut> output = new ArrayList<CptCalculatorAppOut>();

        for (ZTrip trip : trips) {
            List<ZTripStop> stops = trip.getTripStops();
            Collections.sort(stops);
            for (int i = 0; i < stops.size(); i++) {
                for (int j=i+1; j< stops.size(); j++) {
                    ZTripStop start = stops.get(i);
                    ZTripStop end = stops.get(j);
                    output.add(new CptCalculatorAppOut(start, end));
                }
            }
        }
        return output;
    }

    public static List<CptCalculatorAppOut> generateIndirectRoutes(List<ZTrip> trips,
                                                                   List<TripScheduleInput> tripSchedules,
                                                                   LinehaulNetwork linehaulNetwork) {
        Set<String> locations = new HashSet<String>();
        Set<String> directRoutes = new HashSet<String>();
        List<TripLeg> tripLegs = PickupDeliveryAnalyser.getRouteLegs(tripSchedules, linehaulNetwork);
        LinehaulDAG_v2 graph = new LinehaulDAG_v2(tripLegs);

        List<CptCalculatorAppOut> retList = new ArrayList<CptCalculatorAppOut>();

        for(ZTrip trip: trips) {
            List<ZTripStop> stops = trip.getTripStops();
            Collections.sort(stops);
            for(int i=0; i < stops.size(); i++) {
                ZTripStop prevStop = stops.get(i);
                locations.add(prevStop.getLocCode());
                for(int j=i; j < stops.size(); j++) {
                    ZTripStop currStop = stops.get(j);
                    directRoutes.add(prevStop.getLocCode() + "-" + currStop.getLocCode());
                }
            }
            ZTripStop prevStop = null;

            for(ZTripStop currStop : trip.getTripStops()) {

                prevStop = currStop;
            }
        }
        for(String startLoc : locations) {
            for(String endLoc : locations) {
                if(startLoc.equals(endLoc))
                    continue;

                if(directRoutes.contains(startLoc + "-" + endLoc)) {
                    continue;
                }

                log.info(startLoc + "-" + endLoc);
                for(int i=1; i <= 8; i++) {
                    List<List<TripLeg>> options = graph.findPaths(startLoc, endLoc, 0, i * TimeUtils.MILLIS_IN_DAY, ZParams.MAX_TRANSHIPMENTS);
                    if(options != null && options.isEmpty() == false) {
                        for(List<TripLeg> option : options) {
                            retList.add(new CptCalculatorAppOut(option));
                        }
                        break;
                    }
                }
            }
        }
        return retList;
    }


}